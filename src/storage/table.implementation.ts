/*
 * Copyright (c) Clawiz
 */

import {TableColumnImplementation} from "./table.column.implementation";
import {Table, TableColumn} from "../interfaces";

export class TableImplementation implements Table {

  name                     : string;
  columns                  : TableColumn[] = [];
  private _createStatement : string;

  constructor(name : string) {
    this.name = name;
  }

  addColumn(name : string, type : string) : TableColumn {
    let column = new TableColumnImplementation(name, type);
    this.columns.push(column);
    return column;
  }

  protected buildCreateStatement() : string {
    let result = 'create table if not exists ' + this.name + " (";

    let idFound = false;
    for(let column of this.columns ) {
      if ( column.name === 'id') {
        idFound = true;
        break;
      }
    }
    let prefix = '';
    if ( ! idFound ) {
      result += 'id integer primary key autoincrement'
      prefix = ', ';
    }
    for(let i=0; i < this.columns.length; i++) {
      result += prefix + this.columns[i].name + ' ' + this.columns[i].type;
      prefix = ', ';
    }

    result += ')';

    return result;
  }

  get createStatement(): string {
    if ( this._createStatement == null ) {
      this._createStatement = this.buildCreateStatement();
    }
    return this._createStatement;
  }


  set createStatement(value: string) {
    this._createStatement = value;
  }

}
