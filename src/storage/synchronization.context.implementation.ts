
/*
 * Copyright (c) Clawiz
 */

import {SynchronizationContext} from "../interfaces";
export class SynchronizationContextImplementation implements SynchronizationContext {

  showLoading   : boolean = true;
  objectGuid    : string;
  lastLocalScn  : number = 0;
  lastRemoteScn : number = 0;

  rows          : any[]  = [];
}
