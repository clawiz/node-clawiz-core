

/*
 * Copyright (c) Clawiz
 */

import {ApiResponseImplementation} from "../api/api.response.implementation";
import {SynchronizationContextImplementation} from "./synchronization.context.implementation";
import {
  StorageService, DataSourceService, DataObject, DatabaseEngineService,
  SynchronizationContext, ApiResponse, DatabaseType, Loading, ApiProcessPhase, ApiErrorType
} from "../interfaces";
import {AbstractService} from "../service/abstract.service";
import {LocalTableDataSource} from "./datasource/local.table.data.source";
import {LocalForageDatatabaseEngine} from "./database/localforage.database.engine";
import {AbstractDatabaseEngine} from "./database/abstract.database.engine";

export class StorageServiceImplementation extends AbstractService implements StorageService {


  dataSources : DataSourceService<DataObject>[] = [];
  engine      : DatabaseEngineService;


  protected prepareCoreSources() : Promise<any> {
    let ds;

    return this.addDataSource(LocalTableDataSource, "cw_core_config_parameters")
      .then((ds : LocalTableDataSource) => {
        ds = new LocalTableDataSource("cw_core_config_parameters");
        ds.table.addColumn('value',  'text');
        ds.table.addColumn('name',   'text');
        return this.addDataSource(LocalTableDataSource, "cw_core_transaction_actions")
      })
      .then((ds : LocalTableDataSource) => {
        ds.table.addColumn('table_name',    'text');
        ds.table.addColumn('object_id',     'integer');
        ds.table.addColumn('action',        'text');
        return this.addDataSource(LocalTableDataSource, "cw_core_objects")
      })
      .then((ds : LocalTableDataSource) => {
        ds.table.addColumn('table_name',    'text');
        ds.table.addColumn('guid',          'text');
        return Promise.resolve();
      });

  }

  prepare(): Promise<any> {
    return this.prepareCoreSources()
      .then(() => {
        return this.clawiz.getService(LocalForageDatatabaseEngine);
      })
      .then((engine : AbstractDatabaseEngine) => {
        this.engine = engine;
        return this.engine.prepare();
      });
  }

  addDataSource<T extends DataSourceService<DataObject>>(c: {new(...args: any[]): T; }, name? : string) : Promise<T> {
    let dataSource = new c();
    dataSource.clawiz = this.clawiz;
    if ( name != null ) {
      dataSource.name   = name;
    }
    this.dataSources.push(dataSource);
    return dataSource.init()
      .then(() => {
        return Promise.resolve(dataSource);
      });
  }

  private dataSourcesCache;
  getDataSource(name : string) : DataSourceService<DataObject>  {
    if ( name == null ) {
      return null;
    }

    if ( this.dataSourcesCache == null ) {
      let cache = {};
      for (let ds of this.dataSources ) {
        cache[ds.name] = ds;
      }
      this.dataSourcesCache = cache;
    }

    return this.dataSourcesCache[name];

  }

  executeSql(sql : string, ...parameters) {
    return this.engine.executeSql(sql, parameters);
  }


  get(dataSource: DataSourceService<DataObject>, key: string): Promise<any> {
    return this.engine.get(dataSource, key);
  }

  set(dataSource: DataSourceService<DataObject>, key: string, value): Promise<any> {
    return this.engine.set(dataSource, key, value);
  }


  clear(dataSource: DataSourceService<DataObject>): Promise<any> {
    return this.engine.clear(dataSource);
  }

  remove(dataSource: DataSourceService<DataObject>, key: string): Promise<any> {
    return this.engine.remove(dataSource, key);
  }

  keys(dataSource: DataSourceService<DataObject>): Promise<string[]> {
    return this.engine.keys(dataSource);
  }

  private loadChangeData(result : any[], index) {

    if ( index >= result.length) {
      return Promise.resolve(result);
    }

    let me  = this;
    let row = result[index];

    let dataSource = this.getDataSource(row.table_name);
    if ( dataSource == null ) {
      return me.clawiz.getPromiseReject('Wrong datasource name ?', row.table_name);
    }
    if ( row.action === 'DELETE' ) {
      return dataSource.idToGuid(row.id)
        .then((guid)  => {
          row.id = guid;
          return me.loadChangeData(result, index+1);
        });
    }


    return dataSource.load(row.id)
      .then((object) => {

        return dataSource.objectToServerRow(object)
          .then((serverRow) => {
            for (let key in serverRow ) {
              row[key] = serverRow[key];
            }
            return me.loadChangeData(result, index+1);
        })
          .catch((reason) => {
            return me.clawiz.getPromiseReject('Exception on objectToServerRow of ? record ? : ?', row.table_name, row.id, reason.message, reason);
          });

      })
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception on load ? record ? : ?', row.table_name, row.id, reason.message, reason);
      })

  }

  private sqlPrepareLocalChanges(context) {
    let me = this;

    return this.executeSql('select * from cw_core_transaction_actions where id > ? order by id', context.lastLocalScn)
      .then((data) => {
        let result     = [];
        for ( let i=0; i < data.rows.length; i++) {

          let row = data.rows.item(i);

          result.push({
            scn         : row.id,
            id          : row.object_id,
            action      : row.action,
            table_name  : row.table_name
          });

          context.lastLocalScn = row.id;

        }

        return me.loadChangeData(result, 0);
      })
      .catch((reason) => {
        return me.clawiz.getPromiseReject('System exception on load local changes : ?', reason.message, reason);
      });

  }

  _transactionActionsDataSource : DataSourceService<DataObject>;
  private get transactionActionsDataSource() {
    if ( ! this._transactionActionsDataSource ) {
      this._transactionActionsDataSource = this.getDataSource('cw_core_transaction_actions');
    }
    return this._transactionActionsDataSource;
  }

  private nosqlLoadTransactionActions( result : any[], index : number ) : Promise<any> {
    if ( index >= result.length ) {
      return Promise.resolve();
    }

    let me = this;
    return me.get(me.transactionActionsDataSource, result[index].scn+'')
      .then((action) => {
        let row        = result[index];
        row.id         = action.object_id;
        row.action     = action.action;
        row.table_name = action.table_name;
        return me.nosqlLoadTransactionActions(result, index+1);
      })
      .catch((reason) => {
        return Promise.reject(reason);
      })
  }

  private nosqlPrepareLocalChanges(context) {
    let me = this;
    let ds = me.transactionActionsDataSource;

    return this.keys(ds)
      .then((keys : string[]) =>  {

        let result = [];
        for (let key of keys ) {
          let scn = +key;
          if ( scn > context.lastLocalScn ) {
            result.push({
              scn : scn
            });
          }
          if ( scn > context.lastLocalScn ) {
            context.lastLocalScn = scn;
          }
        }
        return me.nosqlLoadTransactionActions(result, 0)
          .then(() => {
            return me.loadChangeData(result, 0);
          })
          .catch((reason) => {
            return me.clawiz.getPromiseReject('System exception on load local changes : ?', reason.message, reason);
          });

      })
      .catch((reason) => {
        return me.clawiz.getPromiseReject('System exception on load local changes : ?', reason.message, reason);
      });

  }


  private saveChanges(context : SynchronizationContext, index ) : Promise<any>{
    if ( index >= context.rows.length ) {
      return Promise.resolve();
    }

    let me  = this;
    let row = context.rows[index];

    // me.clawiz.logInfo("Save synchronize record : ?", JSON.stringify(row));

    let dataSource = this.getDataSource(row.table_name);
    if ( dataSource == null ) {
      return me.clawiz.getPromiseReject('Wrong datasource name ?', row.table_name);
    }

    row['_fromServer'] = true;
    if ( row.action === 'NEW' || row.action === 'CHANGE' ) {

      let currentObject;
      return dataSource.serverRowToObject(row)
        .then((object) => {
          // store server guid in cw_core_objects, not generate new
          row._object                  = object;
          currentObject                = object;
          currentObject['_guid']       = row.id;
          currentObject['_fromServer'] = true;
          return dataSource.save(currentObject)
            .then(() => {
              context.lastRemoteScn = row.scn;
              return me.clawiz.config.setParameter('last_remote_scn', row.scn + '');
            })
            .then(() => {
                return me.saveChanges(context, index+1);
              // }
            });
        })
        .catch((reason) => {
          return Promise.reject(reason)}
        );

    } else if ( row.action === 'DELETE') {

      return dataSource.guidToId(row.id, true, true)
        .then((id) => {
          if ( id != null ) {
            return dataSource.delete(id, true);
          } else {
            return Promise.resolve();
          }
        })
        .then(() => {
          return me.clawiz.config.setParameter('last_remote_scn', row.scn + '');
        })
        .then(() => {
          return me.saveChanges(context, index+1);
        })
        .catch((reason) => {
          return Promise.reject(reason)}
        );

    } else {
      return me.clawiz.getPromiseReject('Wrong synchronize action type ? for row ? of table ?', row.action, row.id, row.table_name)
    }

  }

  public dumpAll(index) {
    let me = this;

    if ( index >= this.dataSources.length) {
      return Promise.resolve();
    }

    return this.dataSources[index].dump()
      .then(() => {
        return me.dumpAll(index+1);
      })

  }

  createCoreObject(dataSource : DataSourceService<DataObject>, guid: string): Promise<number> {
    return this.engine.createCoreObject(dataSource, guid);
  }

  deleteCoreObject(id: number): Promise<any> {
    return this.engine.deleteCoreObject(id);
  }

  saveTransactionAction(dataSource : DataSourceService<DataObject>, objectId : number, action: string): Promise<any> {
    return this.engine.saveTransactionAction(dataSource, objectId, action)
  }

  private stopDataSourceSubcribers() : Promise<any> {

    let promises : Promise<any>[] = [];

    for(let ds of this.dataSources ) {
      promises.push(ds.stopSubscriptions());
    }

    return Promise.all(promises);
  }

  private startDataSourceSubcribers() : Promise<any> {

    let promises : Promise<any>[] = [];

    for(let ds of this.dataSources ) {
      promises.push(ds.startSubscriptions());
    }

    return Promise.all(promises);
  }

  private _inSynchronize   = false;

  private loadingOrNull(showLoading : boolean ) : Promise<Loading> {
    if ( showLoading ) {
      return this.clawiz.ui.showLoading();
    } else {
      return Promise.resolve(null)
    }
  }

  private dismissLoading(loading : Loading) : Promise<any> {
    return loading != null ? loading.hide() : Promise.resolve();
  }

  private getErrorResolve(errorType : ApiErrorType, processPhase : ApiProcessPhase, errorMessage : string) : Promise<ApiResponse> {
    let response = new ApiResponseImplementation();
    response.success      = false;
    response.errorType    = errorType;
    response.message = errorMessage;
    response.processPhase = processPhase;
    return Promise.resolve(response);
  }

  synchronize(_context?: SynchronizationContext) : Promise<ApiResponse> {

    if ( ! this.clawiz.config.storage.synchronizationEnabled ) {
      let response = new ApiResponseImplementation();

      return Promise.resolve(response);
    }

    let context = new SynchronizationContextImplementation();
    for (let key in _context) {
      context[key] = _context[key];
    }

    if (this._inSynchronize && context.objectGuid == null) {
      return this.getErrorResolve(ApiErrorType.INTERNAL_ERROR, ApiProcessPhase.PREPARE_CALL, 'Concurrent call of synchronize is prohibited');
    }
    if ( context.objectGuid == null ) {
      this._inSynchronize = true;
    }

    // this.clawiz.logInfo("Synchronization started with " + JSON.stringify(context));

    let loading  : Loading;
    let response : ApiResponse;

    return this.loadingOrNull(context.showLoading)
      .then((_loading) => {
        loading = _loading;
        return context.objectGuid == null ? this.stopDataSourceSubcribers() : Promise.resolve(0);
      })
      .then(() => {
        return context.objectGuid == null ? this.clawiz.config.getNumberParameter('last_remote_scn') : Promise.resolve(0);
      })
      .then((scn) => {
        context.lastRemoteScn = scn;

        return context.objectGuid == null ? this.clawiz.config.getNumberParameter('last_local_scn') : Promise.resolve(0);
      })
      .then((scn) => {
        context.lastLocalScn = scn;

        if ( context.objectGuid == null ) {
          if (this.engine.databaseType == DatabaseType.SQL) {
            return this.sqlPrepareLocalChanges(context)
          } else if (this.engine.databaseType == DatabaseType.NoSQL) {
            return this.nosqlPrepareLocalChanges(context)
          } else {
            return this.clawiz.getPromiseReject("Unsupported local database type ? ", DatabaseType[<number>this.engine.databaseType]);
          }
        } else {
          return Promise.resolve([]);
        }

      })
      // .then((data) => {
      //   return this.dumpAll(0);
      // })
      .then((data) => {
        let params = {
          firstServerScn : context.lastRemoteScn + 1,
          data           : data
        };
        if ( context.objectGuid != null ) {
          params['objectId'] = context.objectGuid;
        }
        return this.clawiz.api.post(
          {

            path                 : this.clawiz.config.storage.dataSourceApiPath,
            params               : params,
            synchronizeAfterCall : false,
            showLoading          : false,
            showErrorAlert       : true
          }
        )
      })
      .then((_response: ApiResponse) => {
        response = _response;
        if (response.success) {
          if (response['rows'] != null) {
            context.rows = response['rows'];
            if ( _context != null ) {
              _context.rows = context.rows;
            }
            return this.saveChanges(context, 0);
          } else {
            return Promise.resolve();
          }
        } else {
          if (response.errorType == ApiErrorType.NETWORK_ERROR) {
            this.clawiz.logError('Server connect error');
          } else {
            this.clawiz.logError('Api exception ?', response.logErrorMessage);
          }
          return Promise.reject(response);
        }
      })
      .then(() => {
        return context.objectGuid == null ? this.clawiz.config.setParameter('last_local_scn', context.lastLocalScn + '') : Promise.resolve();
      })
      .then(() => {
        return context.objectGuid == null ? this.startDataSourceSubcribers() : Promise.resolve();
      })
      .then(() => {
        return context.objectGuid == null ? this.clawiz.node.prepare() : Promise.resolve();
      })
      // .then(() => {
      //   return this.dumpAll(0);
      // })
      .then(() => {
        return this.dismissLoading(loading);
      })
      .then(() => {
        if ( context.objectGuid == null ) {
          this._inSynchronize = false;
        }
        return Promise.resolve(response);
      })
      .catch((reason) => {
        this._inSynchronize = false;
        return this.dismissLoading(loading)
          .then(() => {

            if (reason.request != null) {
              if (reason.errorType == ApiErrorType.NETWORK_ERROR) {
                return Promise.resolve(reason);
              }
            }

            let error = reason.request != null ? reason.logErrorMessage : reason.message;
            this.clawiz.logError('Synchronization failed with ?', error);
            this.clawiz.ui.showErrorAlert(
              'Непредвиденная ошибка. Пожалуйста, войдите в программу еще раз'
              , this.reloadData.bind(this)
            );
            return this.getErrorResolve(ApiErrorType.INTERNAL_ERROR, ApiProcessPhase.SYNCHRONIZE_AFTER_CALL, error);
          });

      });

  }

  reloadData() {
    return this.clawiz.node.logoutUser();
  }

}
