/*
 * Copyright (c) Clawiz
 */

import {TableColumn} from "../interfaces";
/**
 * Created by abdrashitovta on 20.03.2017.
 */

export class TableColumnImplementation implements TableColumn {

  name      : string;
  type      : string;


  constructor(name: string, type: string) {
    this.name = name;
    this.type = type;
  }
}
