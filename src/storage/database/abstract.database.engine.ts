/*
 * Copyright (c) Clawiz
 */

import {DatabaseEngineService, DatabaseType, DataObject, DataSourceService} from "../../interfaces";
import {ClawizImplementation} from "../../builder/clawiz.implementation";
import {AbstractService} from "../../service/abstract.service";
export abstract class AbstractDatabaseEngine extends AbstractService implements DatabaseEngineService {

  get databaseType() : DatabaseType {
    return DatabaseType.SQL;
  }


  abstract prepare(): Promise<any>;

  abstract makeClearDatabase() : Promise<any>;
  clearDatabase() : Promise<any> {
    this.clawiz.logInfo('Clearing database ...');
    return this.makeClearDatabase()
      .catch((reason) => {
        return this.clawiz.getPromiseReject('Clear database exception : ?', reason.message, reason);
      })
  }

  abstract executeSql(sql: string, ...parameters) : Promise<any>;

  abstract get(dataSource: DataSourceService<DataObject>, key: string) : Promise<any>;
  abstract set(dataSource: DataSourceService<DataObject>, key: string, value) : Promise<any>;
  abstract clear(dataSource : DataSourceService<DataObject>) : Promise<any>;
  abstract remove(dataSource : DataSourceService<DataObject>, key : string) : Promise<any>;
  abstract keys(dataSource : DataSourceService<DataObject>): Promise<string[]>;


  abstract createCoreObject(dataSource : DataSourceService<DataObject>, guid: string): Promise<number>;
  abstract deleteCoreObject(id: number): Promise<any>;
  abstract saveTransactionAction(dataSource : DataSourceService<DataObject>, objectId: number, action: string): Promise<any>;

  protected prepareParametersArray(sourceParameters : any[], destinationParameters : any[]) {
    for (let p of sourceParameters ) {
      if ( p instanceof Array ) {
        this.prepareParametersArray(p, destinationParameters);
      } else {
        destinationParameters.push(p);
      }
    }
  }

}
