
/*
 * Copyright (c) Clawiz
 */

import {AbstractDatabaseEngine} from "./abstract.database.engine";

import * as localForage from "localforage";
import {DatabaseType, DataObject, DataSourceService} from "../../interfaces";

export class LocalForageDatatabaseEngine extends AbstractDatabaseEngine {

  instances            : any[] = [];

  private lastObjectId            : number;
  private lastTransactionActionId : number;

  get databaseType(): DatabaseType {
    return DatabaseType.NoSQL;
  }

  private clearDatasources(index : number) {
    let me = this;
    if ( index >= me.clawiz.storage.dataSources.length ) {
      return Promise.resolve();
    }

    return me.clear(me.clawiz.storage.dataSources[index])
      .then(() => {
        return me.clearDatasources(index+1);
      });
  }

  makeClearDatabase(): Promise<any> {
    return this.clearDatasources(0);
  }


  private prepareInstances() {

    let me      = this;

    for (let ds of me.clawiz.storage.dataSources) {
      let name = ds.table.name;
      // me.clawiz.logInfo("Creating localForge instance for ?", name);
      let instance = localForage.createInstance({
        name : name
      });
      this.instances[name] = instance;
    }

  }

  prepare(): Promise<any> {
    let me = this;
    me.prepareInstances();
    return this.clawiz.config.getNumberParameter('last_object_id')
      .then((id) => {
        me.lastObjectId = id != null ? id : 0;
        return me.clawiz.config.getNumberParameter('last_transaction_action_id');
      })
      .then((id) => {
        me.lastTransactionActionId = id != null ? id : 0;
        return Promise.resolve();
      });

  }


  executeSql(sql: string, ...parameters)  : Promise<any> {
    return this.clawiz.getPromiseReject('Execute sql not applicable for localForage engine : ?', sql, parameters);
  }

  protected getInstance(dataSource : DataSourceService<DataObject>)  {
    if ( dataSource == null ) {
      return this.clawiz.getPromiseReject('Datasource cannot be null');
    }

    let instance = this.instances[dataSource.name];

    if ( instance != null ) {
      return instance;
    };

    throw this.clawiz.newError("Unknown datasource ?", dataSource.name);

  }

  get(dataSource: DataSourceService<DataObject>, key: string): Promise<any> {

    if ( key == null ) {
      return Promise.resolve(null);
    }

    let me = this;
    return this.getInstance(dataSource).getItem(key)
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception on get datasource ? key ? value : ?', dataSource.name, key, reason.message, reason);
      });

  }

  set(dataSource: DataSourceService<DataObject>, key: string, value): Promise<any> {

    if ( key == null ) {
      return this.clawiz.getPromiseReject('Cannot set datasource ? value for null key', dataSource.name);
    }

    if ( value == null ) {
      return this.remove(dataSource, key);
    }

    let me = this;
    return this.getInstance(dataSource).setItem(key, value)
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception on set datasource ? key ? value ? : ?', dataSource.name, key, value, reason.message, reason);
      });

  }

  clear(dataSource: DataSourceService<DataObject>): Promise<any> {
    let me = this;
    return this.getInstance(dataSource).clear()
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception on clear datasource ? : ?', dataSource.name, reason.message, reason);
      });
  }

  remove(dataSource: DataSourceService<DataObject>, key: string): Promise<any> {
    if ( key == null ) {
      return this.clawiz.getPromiseReject('Cannot remove datasource ? value for null key', dataSource.name);
    }

    let me = this;
    return this.getInstance(dataSource).removeItem(key)
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception on get remove datasource ? key ? value : ?', dataSource.name, key, reason.message, reason);
      });
  }

  keys(dataSource: DataSourceService<DataObject>): Promise<string[]> {
    let me = this;
    return this.getInstance(dataSource).keys()
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception on get remove datasource ? keys : ?', dataSource.name, reason.message, reason);
      });
  }


  _coreObjectsDataSource : DataSourceService<DataObject>;
  get coreObjectsDataSource() {
    if ( ! this._coreObjectsDataSource ) {
      this._coreObjectsDataSource = this.clawiz.storage.getDataSource('cw_core_objects');
    }
    return this._coreObjectsDataSource;
  }

  _transactionActionsDataSource : DataSourceService<DataObject>;
  get transactionActionsDataSource() {
    if ( ! this._transactionActionsDataSource ) {
      this._transactionActionsDataSource = this.clawiz.storage.getDataSource('cw_core_transaction_actions');
    }
    return this._transactionActionsDataSource;
  }


  createCoreObject(dataSource : DataSourceService<DataObject>, guid: string): Promise<number> {
    let me  = this;
    let id  = ++this.lastObjectId;
    let row = {
      id         : id,
      table_name : dataSource.table.name,
      guid       : guid
    };
    return me.set(me.coreObjectsDataSource, id+'', row)
      .then(() => {
        return me.set(me.coreObjectsDataSource, guid, row);
      })
      .then(() => {
        return me.clawiz.config.setParameter('last_object_id', id+'');
      })
      .then(() => {
        return Promise.resolve(id);
      })
      .catch((reason) => {
        return Promise.reject(reason);
      });

  }

  deleteCoreObject(id: number): Promise<any> {
    let guid;
    let me  = this;
    return me.get(me.coreObjectsDataSource, id+'')
      .then((row) => {
        if ( row == null ) {
          return me.clawiz.getPromiseReject('Unknown object id ?', id);
        }
        guid = row.guid;
        return me.remove(this.coreObjectsDataSource, id+'')
          .then(() => {
            return me.remove(this.coreObjectsDataSource, guid);
          })
      })

//
  }

  saveTransactionAction(dataSource : DataSourceService<DataObject>, objectId: number, action: string): Promise<any> {
    let me = this;
    let id = ++this.lastTransactionActionId;

    return me.set(me.transactionActionsDataSource, id+'', {
      id         : id,
      table_name : dataSource.table.name,
      object_id  : objectId,
      action     : action
    })
      .then(() => {
        return me.clawiz.config.setParameter('last_transaction_action_id', id+'');
      })
      .catch((reason) => {
        return Promise.reject(reason);
      });
  }
}
