/*
 * Copyright (c) Clawiz
 */

import {AbstractDataSourceImplementation} from "./abstract.data.source.implementation";
import {AbstractDataObjectImplementation} from "./abstract.data.object.implementation";

export class LocalTableDataSource extends AbstractDataSourceImplementation<AbstractDataObjectImplementation> {

  constructor(name : string) {
    super();
    this.name = name;
  }

  serverRowToObject(row: any) {
    return Promise.resolve(null);
  }

}
