/*
 * Copyright (c) Clawiz
 */

import {DataModel, DataModelField, DataSourceService, DataObject} from "../../interfaces";
export class AbstractDataModelImplementation implements DataModel {

  fields: DataModelField[] = [];


  init() {
  }

}
