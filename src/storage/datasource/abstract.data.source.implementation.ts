/*
 * Copyright (c) Clawiz
 */

import {TableImplementation} from "../table.implementation";
import {SynchronizationContextImplementation} from "../synchronization.context.implementation";
import {
  DataObject, DataSourceService, Clawiz, Table, DataSourceSubscribe, DatabaseType,
  StorageService, ApiResponse, QueryContext, QueryOrderDirection, DataSourceSubscribeActionType, DataModel
} from "../../interfaces";
import {AbstractService} from "../../service/abstract.service";

export abstract class AbstractDataSourceImplementation<M extends DataObject> extends AbstractService implements DataSourceService<M> {

  private _name          : string;
  dataSourceApiPath : string;

  table                  : Table = new TableImplementation(null);

  subscribes             : DataSourceSubscribe[] = [];

  get model(): DataModel {
    return null;
  }

  private _databaseType : DatabaseType;
  get databaseType() : DatabaseType {
    if ( this._databaseType == null ) {
      this._databaseType = this.clawiz.storage.engine.databaseType;
    }
    return this._databaseType;
  }

  private _storage : StorageService;
  get storage() : StorageService {
    if ( this._storage == null ) {
      this._storage = this.clawiz.storage;
    }
    return this._storage;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name      = value;
    this.table.name = value;
  }

  private _idCache       = {};

  private putToIdCache(data : M) {
    if ( data == null ) {
      throw this.clawiz.newError('Cannot put to cache ? null data object', this.name);
    }
    if ( data.id == null ) {
      throw this.clawiz.newError('Cannot put to cache ? object with null id', this.name);
    }
    this._idCache[data.id] = data;
  }

  private getFromIdCache(id : number) : M {
    return this._idCache[id];
  }

  public removeFromIdCache(id : number)  {
    this._idCache[id] = null;
  }

  _coreObjectsDataSource : DataSourceService<DataObject>;
  get coreObjectsDataSource() {
    if ( ! this._coreObjectsDataSource ) {
      this._coreObjectsDataSource = this.clawiz.storage.getDataSource('cw_core_objects');
    }
    return this._coreObjectsDataSource;
  }

  idToGuid(id : number) : Promise<string> {
    if ( ! id ) {
      return Promise.resolve(null);
    }

    let me = this;


    if ( me.databaseType == DatabaseType.SQL) {

      return me.executeSql("select guid from cw_core_objects where id = ?", id)
        .then((data) => {
          if ( data.rows.length > 0 ) {
            return Promise.resolve(data.rows.item(0).guid);
          } else {
            return me.clawiz.getPromiseReject('Unknown object id ?', id);
          }
        });

    } else if ( me.databaseType == DatabaseType.NoSQL ) {

      return me.storage.get(me.coreObjectsDataSource, id+'')
        .then((row) => {
          if ( row != null ) {
            return Promise.resolve(row.guid);
          } else {
            return me.clawiz.getPromiseReject('Unknown object id ?', id);
          }
        })

    } else {
      return this.clawiz.getPromiseReject("Unsupported datasource idToGuid database type ? ", DatabaseType[<number>this.databaseType]);
    }

  }

  private requestObjectFromServer(guid : string, nullIfNotFound : boolean) : Promise<any> {
    if ( nullIfNotFound ) {
      return Promise.resolve(null);
    }
    let context = new SynchronizationContextImplementation();
    context.showLoading = false;
    context.objectGuid  = guid;
    return this.clawiz.storage.synchronize(context)
      .then((response : ApiResponse) => {

        if ( ! response.success ) {
          return Promise.reject(response);
        }
        let id : number = null;
        for (let i=0; i < context.rows.length; i++) {
          if (context.rows[i].id == guid) {
            id = context.rows[i]._object.id;
            break;
          }
        }
        if ( id == null ) {
          return nullIfNotFound ? Promise.resolve(null) : this.clawiz.getPromiseReject('Unknown object guid ?', guid);
        }
        return Promise.resolve(id);
      })
      .catch((reason) => {
        this.clawiz.logError('exception on request object from server by guid ? : ?', guid, reason.message);
        return Promise.reject(reason);
      });
  }

  guidToId(guid : string, nullIfNotFound? : boolean, skipServerRequest? : boolean) : Promise<number> {
    if ( guid == null ) {
      return Promise.resolve(null);
    }
    let me = this;

    if ( me.databaseType == DatabaseType.SQL) {
      return me.executeSql("select id from cw_core_objects where guid = ?", guid)
        .then((data) => {
          if ( data.rows.length > 0 ) {
            return Promise.resolve(data.rows.item(0).id);
          } else if ( (nullIfNotFound && skipServerRequest) ) {
            return Promise.resolve();
          } else {
            return this.requestObjectFromServer(guid, nullIfNotFound);
          }
        });
    } else if ( me.databaseType == DatabaseType.NoSQL ) {

      return me.storage.get(me.coreObjectsDataSource, guid+'')
        .then((row) => {
          if ( row != null  ) {
            return Promise.resolve(row.id);
          } else if ( (nullIfNotFound && skipServerRequest) ) {
            return Promise.resolve();
          } else {
            return this.requestObjectFromServer(guid, nullIfNotFound);
          }
        })

    } else {
      return this.clawiz.getPromiseReject("Unsupported datasource guidToId database type ? ", DatabaseType[<number>this.databaseType]);
    }
  }

  serverRowToObject(row: any) : Promise<M> {
    return this.clawiz.getPromiseReject("serverRowToObject method not implemented");
  }

  objectToServerRow(object: M) : Promise<any> {
    return this.clawiz.getPromiseReject("objectToServerRow method not implemented");
  }

  executeSql(sql : string, ...parameters) {
    return this.clawiz.storage.executeSql(sql, parameters);
  }

  sqlLoad(id: number) : Promise<M> {
    return this.clawiz.getPromiseReject("sqlLoad method not implemented");
  }

  nosqlLoad(id: number) : Promise<M> {
    let me = this;
    return me.storage.get(me, id + '')
      .then((data) => {
        return me.serverRowToObject(data);
      })
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception on load datasource ? row ? : ?', me.name, id, reason.message, reason);
      });
  }


  makeLoad(id: number) : Promise<M> {

    if ( this.databaseType == DatabaseType.SQL ) {
      return this.sqlLoad(id);
    } else if ( this.databaseType == DatabaseType.NoSQL ) {
      return this.nosqlLoad(id);
    } else {
      return this.clawiz.getPromiseReject("Unsupported datasource loadOrderedList database type ? ", DatabaseType[<number>this.databaseType]);
    }

  }


  prepareObject(object: M): Promise<any> {
    return Promise.resolve();
  }

  load(id: number) : Promise<M> {

    if ( ! id || id < 0  ) {
      return Promise.resolve(null);
    }
    let me = this;

    let data = me.getFromIdCache(id)
    if ( data != null ) {
      return Promise.resolve(data);
    }

    return this.makeLoad(id)
      .then((data : M) => {
        me.putToIdCache(data);
        return me.prepareObject(data)
          .then(() => {
            return Promise.resolve(data);
          });
      })
      .catch((reason) => {
        return Promise.reject(reason)
      });

  }

  sqlLoadList(queryContext : QueryContext) : Promise<M[]> {
    return this.clawiz.getPromiseReject("sqlLoadOrderedList method not implemented");
  }

  nosqlLoadList(queryContext : QueryContext) : Promise<M[]> {

    let me           = this;
    let result : M[] = [];

    return me.storage.keys(me)
      .then((keys : string[]) => {

        let all    : Promise<any>[] = [];

        for ( let key of keys) {

          all.push(me.load(+key)
            .then((row : M ) => {
              if ( row != null ) {
                result.push(row);
              }
            }));

        }

        return Promise.all(all);

      })
      .then(() => {
        return Promise.resolve(result);
      })
      .catch((reason) => {
        return Promise.reject(reason);
      })

  }

  private putListToIdCache(list : M[]){

    for (let data of list ) {
      this.putToIdCache(data);
    }

  }

  private prepareListObjects(list : M[], index : number) : Promise<M[]> {
    if ( index >= list.length ) {
      return Promise.resolve(list);
    }

    let me = this;

    return me.prepareObject(list[index])
      .then(() => {
        return me.prepareListObjects(list, index+1);
      })
      .catch((reason) => {
        return Promise.reject(reason);
      });
  }

  private applyQueryContextFilters(list : M[], queryContext : QueryContext) : Promise<M[]> {
    let result : M[] = [];
    for (let i=0; i < list.length; i++) {
      let passed = true;
      let row    = list[i];
      for (let c of queryContext.conditions ) {
        if ( c.filter != null && ( ! c.filter(row)) ) {
          passed = false;
          break;
        }
      }
      if ( passed ) {
        result.push(list[i]);
      }
    }
    return Promise.resolve(result);
  }

  private compare(o1, o2) : number {
    if ( o1 == null || o2 == null ) {
      return 0;
    }
    let v1 = o1;
    let v2 = o2;
    if ( o1 instanceof Date ) {
      v1 = o1.getTime();
      v2 = o2.getTime();
    }

    if ( v1 > v2 ) {
      return 1;
    }
    if ( v1 < v2 ) {
      return -1;
    }
    return 0;
  }

  private applyQueryContextOrders(list : M[], queryContext : QueryContext) : Promise<M[]> {

    let me = this;

    list.sort((o1, o2) : number => {
      for (let order of queryContext.orders) {
        let v1      = o1[order.columnName];
        let v2      = o2[order.columnName];
        let result = me.compare(v1, v2);
        if (result != 0) {
          return (order.direction == null || order.direction == QueryOrderDirection.ASC) ? result : -result;
        }
      }
      return 0;
    });


    return Promise.resolve(list);
  }

  loadList(queryContext : QueryContext) : Promise<M[]> {

    let me   = this;

    let promise : Promise<M[]>;
    if ( this.databaseType == DatabaseType.SQL ) {
      promise = this.sqlLoadList(queryContext);
    } else if ( this.databaseType == DatabaseType.NoSQL ) {
      promise = this.nosqlLoadList(queryContext);
    } else {
      return this.clawiz.getPromiseReject("Unsupported datasource loadOrderedList database type ? ", DatabaseType[<number>this.databaseType]);
    }

    return promise
      .then((list : M[]) => {
        return me.applyQueryContextFilters(list, queryContext);
      })
      .then((list : M[]) => {
        return me.applyQueryContextOrders(list, queryContext);
      })
      .then((list : M[]) => {
        me.putListToIdCache(list);
        return me.prepareListObjects(list, 0)
      })
      .catch((reason) => {
        return Promise.reject(reason);
      });

  }

  sqlSave(object: M, asNew : boolean) : Promise<any> {
    return this.clawiz.getPromiseReject("sqlSave method not implemented");
  }

  private checkObjectId(object : M) : Promise<any> {
    if ( object.id ) {
      return Promise.resolve(object.id);
    }
    let guid   = object['_guid'] ? object['_guid'] : this.stringUtils.newGuid();
    return this.storage.createCoreObject(this, guid)
      .then((id => {
        object.id = id;
        return Promise.resolve(id);
      }))
      .catch((reason) => {
        return Promise.reject(reason)
      });
  }

  nosqlSave(object: M) : Promise<any> {
    let me = this;
    if ( object == null ) {
      return me.clawiz.getPromiseReject('Cannot save null data to ?', me.name);
    }

    return me.objectToServerRow(object)
      .then((row) => {
        row.id = object.id;
        // return original id, because server objectToServerRow transform id to external guid
        return me.storage.set(me, object.id + '', row);
      });
  }

  save(object: M) : Promise<any> {

    let fromServer : boolean = object['_fromServer'];
    object['_fromServer'] = null;
    let asNew            = object.id == null ? true : false;
    let action           = asNew ? 'NEW' : 'CHANGE';

    return this.prepareObject(object)
      .then(() => {
        return this.checkObjectId(object)
      })
      .then(() => {
        if ( this.databaseType == DatabaseType.SQL ) {
          return this.sqlSave(object, asNew);
        } else if ( this.databaseType == DatabaseType.NoSQL ) {
          return this.nosqlSave(object);
        } else {
          return this.clawiz.getPromiseReject("Unsupported datasource save database type ? ", DatabaseType[<number>this.databaseType]);
        }
      })
      .then(() => {
        return this.putToIdCache(object);
      })
      .then(() => {
        if ( ! fromServer ) {
          return this.storage.saveTransactionAction(this, object.id, action);
        } else {
          return Promise.resolve();
        }
      })
      .then(() => {
        return this.processSubscribers();
      })
      .then(() => {
        if ( ! fromServer ) {
          return this.clawiz.storage.synchronize({
            showLoading : false
          })
            .then((response : ApiResponse) => {
              return response.success ? Promise.resolve() : Promise.reject(response);
            });
        } else {
          return Promise.resolve();
        }
      })
      .catch((reason) => {
        return Promise.reject(reason);
      })


  }

  sqlDelete(id: number) : Promise<any> {
    let me = this;

    return me.executeSql('delete from ' + me.table.name  + ' where id = ?', id)
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception on delete row ? from table ? : ?', id, me.table.name, reason.message, reason);
      })

  }

  nosqlDelete(id: number) : Promise<any> {
    return this.storage.remove(this, id + '');
  }

  private makeDelete(id : number) : Promise<any> {
    if ( this.databaseType == DatabaseType.SQL ) {
      return this.sqlDelete(id);
    } else if ( this.databaseType == DatabaseType.NoSQL ) {
      return this.nosqlDelete(id);
    } else {
      return this.clawiz.getPromiseReject("Unsupported datasource delete database type ? ", DatabaseType[<number>this.databaseType]);
    }
  }

  delete(id: number, skipSaveTransactionAction? :boolean) : Promise<any> {

    let me = this;


    return me.makeDelete(id)
      .then(() => {
        me.removeFromIdCache(id);
        // Don not uncomment ! Core objects data need to synchronize deleted objects
        // (unlike server we don't have audit tables)
        // return me.storage.deleteCoreObject(id);
        return Promise.resolve();
      })
      .then(() => {
        if ( ! skipSaveTransactionAction ) {
          return me.storage.saveTransactionAction(this, id, 'DELETE');
        } else {
          return Promise.resolve();
        }
      })
      .then(() => {
        return me.processSubscribers();
      })
      .then(() => {
        return me.clawiz.storage.synchronize({
          showLoading : false
        })
          .then((response : ApiResponse) => {
            return response.success ? Promise.resolve() : Promise.reject(response);
          });
      })
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception on delete object ? from table ? : ?', id, me.table.name, reason.message, reason);
      });

  }

  public sqlDump() : Promise<any> {
    let me = this;

    return me.clawiz.storage.executeSql('select * from ' + me.table.name + ' order by id')
      .then((data) => {
        for (let i=0; i < data.rows.length; i++){
          let str    = '[';
          let prefix = '';
          let row    = data.rows.item(i);
          if ( this.table.columns[0].name != 'id') {
            str += prefix + "'" + row.id + "'";
            prefix = ', ';
          }
          for (let column of this.table.columns) {
            str += prefix +
              (column.name ? "'" + row[column.name] + "'" : 'undefined');
            prefix = ', ';
          }
          this.clawiz.logInfo(str + ']');
        }
        return Promise.resolve();
      })
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception of dump datasource ? : ?', me.name, reason.message, reason);
      })

  }

  private nosqlDumpKey(keys : string[], index : number) : Promise<any> {
    if ( index >= keys.length ) {
      return Promise.resolve();
    }

    let me = this;

    return me.storage.get(this, keys[index])
      .then((row) => {
        me.clawiz.logInfo(keys[index] + ' : ' + JSON.stringify(row));
        return me.nosqlDumpKey(keys, index+1);
      })
  }

  nosqlDump() : Promise<any> {

    let me = this;
    return me.clawiz.storage.keys(this)
      .then((keys : string[]) => {
        return me.nosqlDumpKey(keys, 0);
      })
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception of dump datasource ? : ?', me.name, reason.message, reason);
      })

  }


  dump() : Promise<any> {

    let me = this;

    me.clawiz.logInfo('-----------------------------------------------------');
    me.clawiz.logInfo('            ' + me.name + ' dump');
    me.clawiz.logInfo('-----------------------------------------------------');
    let str    = '';
    let prefix = '';
    for (let column of me.table.columns) {
      str += prefix + "" + column.name + "";
      prefix = ', ';
    }
    me.clawiz.logInfo(str );
    me.clawiz.logInfo('-----------------------------------------------------');

    let promise;

    if ( this.databaseType == DatabaseType.SQL ) {
      promise = this.sqlDump();
    } else if ( this.databaseType == DatabaseType.NoSQL ) {
      promise = this.nosqlDump();
    } else {
      return this.clawiz.getPromiseReject("Unsupported datasource dump database type ? ", DatabaseType[<number>this.databaseType]);
    }

    return promise
      .then(() => {
        me.clawiz.logInfo('-----------------------------------------------------');
        return Promise.resolve();
      })

  }


  private subscribersCount = 0;
  subscribe(subscribe: DataSourceSubscribe) : Promise<any> {
    if ( subscribe['_index']) {
      throw this.clawiz.newError('Subscribe already have subscribe index and cannot be added again');
    }
    subscribe['_index'] = this.subscribersCount++;
    this.subscribes.push(subscribe);
    // this.clawiz.logInfo('data source ? has ? subscribers', this.table.name, this.subscribes.length);
    return this.executeSubscribe(subscribe);
  }

  unsubscribe(subscribe: DataSourceSubscribe) : Promise<any> {

    let index : number = subscribe['_index'];
    for (let i=0; i < this.subscribes.length; i++) {
      if ( this.subscribes[i]['_index'] == index ) {
        subscribe['_index'] = undefined;
        this.subscribes.splice(i, 1);
        return;
      }
    }

  }

  private subscriptionsActive    = true;
  private needProcessSubscribers = false;

  stopSubscriptions() : Promise<any> {
    this.subscriptionsActive = false;
    return Promise.resolve();
  }

  startSubscriptions() : Promise<any> {
    this.subscriptionsActive = true;
    if ( this.needProcessSubscribers ) {
      return this.processSubscribers()
    } else {
      return Promise.resolve();
    }
  }


  protected processSetObjectValueAction(subscribe: DataSourceSubscribe) : Promise<any> {
    if ( subscribe.queryContext.id == null ) {
      return this.clawiz.getPromiseReject('Id not defined for SET_OBJECT_VALUE action of datasource ?', this.name);
    }
    return this.load(subscribe.queryContext.id)
      .then((data) => {
        let result = subscribe.callback(data);
        return result != null ? result : Promise.resolve();
      })
  }

  protected processSetArrayValueAction(subscribe: DataSourceSubscribe) : Promise<any> {
   return this.loadList(subscribe.queryContext)
     .then((data : M[]) => {
       let result = subscribe.callback(data);
       return result != null ? result : Promise.resolve();
    });
  }

  public executeSubscribe(subscribe: DataSourceSubscribe) : Promise<any> {
    if ( ! this.subscriptionsActive ) {
      this.needProcessSubscribers = true;
      return Promise.resolve();
    }

    if (subscribe.actionType == DataSourceSubscribeActionType.ON_CHANGE_SET_OBJECT_VALUE) {
      return this.processSetObjectValueAction(subscribe);
    } else if (subscribe.actionType == DataSourceSubscribeActionType.ON_CHANGE_SET_ARRAY_VALUE) {
      return this.processSetArrayValueAction(subscribe);
    } else {
      return this.clawiz.getPromiseReject('Wrong subscribe action ?', DataSourceSubscribeActionType[+subscribe.actionType]);
    }

  }

  protected processSubscribers() : Promise<any> {
    if ( ! this.subscriptionsActive ) {
      this.needProcessSubscribers = true;
      return Promise.resolve();
    }

    let promises : Promise<any>[] = [];
    for ( let subscribe of this.subscribes ) {
      promises.push(this.executeSubscribe(subscribe));
    }


    return Promise.all(promises);
  }

}
