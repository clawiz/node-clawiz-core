
/*
 * Copyright (c) Clawiz
 */


import {DataObject} from "../../interfaces";

export class AbstractDataObjectImplementation implements DataObject {

  id: number;

}
