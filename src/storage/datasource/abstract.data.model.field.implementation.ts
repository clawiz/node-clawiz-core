/*
 * Copyright (c) Clawiz
 */

import {DataModelField} from "../../interfaces";
export class AbstractDataModelFieldImplementation implements DataModelField {


  name:       string;
  columnName: string;
}
