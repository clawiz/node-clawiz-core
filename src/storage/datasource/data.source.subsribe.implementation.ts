/*
 * Copyright (c) Clawiz
 */

import {QueryContextImplementation} from "./query/query.context.implementation";
import {DataSourceService, DataSourceSubscribe, DataSourceSubscribeActionType, QueryContext} from "../../interfaces";


export class DataSourceSubscribeImplementation implements DataSourceSubscribe {

  dataSource   : DataSourceService<any>;
  actionType   : DataSourceSubscribeActionType;
  queryContext : QueryContext = new QueryContextImplementation();
  object       : any;
  callback     : (obj : any) => Promise<any>;
}
