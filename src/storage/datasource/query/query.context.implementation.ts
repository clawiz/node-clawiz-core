
/*
 * Copyright (c) Clawiz
 */

import {QueryContext, QueryOrder, QueryCondition} from "../../../interfaces";
export class QueryContextImplementation implements QueryContext {

  orders     : QueryOrder[] = [];

  conditions : QueryCondition[] = [];

  id         : number;

  addCondition(condition : QueryCondition) {
    this.conditions.push(condition);
  }

  addOrder(order : QueryOrder) {
    this.orders.push(order);
  }

}
