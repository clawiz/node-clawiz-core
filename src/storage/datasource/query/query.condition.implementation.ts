

/*
 * Copyright (c) Clawiz
 */

import {DataObject, QueryCondition} from "../../../interfaces";
export class QueryConditionImplementation implements QueryCondition {


  filter: (row: DataObject) => boolean;

}
