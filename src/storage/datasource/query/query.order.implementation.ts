

/*
 * Copyright (c) Clawiz
 */

import {QueryOrder, QueryOrderDirection} from "../../../interfaces";
export class QueryOrderImplementation implements QueryOrder {

  columnName    : string;
  direction?    : QueryOrderDirection = QueryOrderDirection.ASC;
}
