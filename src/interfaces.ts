
/*
 * Copyright (c) Clawiz
 */

export interface ApplicationConfig {
  name?         : string;
}

export interface StorageConfig {

  dataSourceApiPath?      : string;
  synchronizationEnabled? : boolean;

}

export interface ApiConfig {
  url?                  : string;
}

export interface UiConfig {
}

export interface ClawizConfig {

  application?             : ApplicationConfig;
  storage?                 : StorageConfig;
  api?                     : ApiConfig;
  ui?                      : UiConfig;
  locale?                  : string;


  getParameter?(name : string);
  getNumberParameter?(name : string);
  setParameter?(name : string, value : string);

}


export interface Service {

  clawiz : Clawiz;
  init() : Promise<any>;

}

export interface ClawizError extends Error {

  reason     : Error;
  parameters : any[];

}

export interface Clawiz {

  config  : ClawizConfig;
  api     : ApiService;
  ui      : UiService;
  storage : StorageService;
  node    : NodeService;

  init() : Promise<any>;

  getService<T extends Service>(c: {new(): T; }) : Promise<T>;

  logInfo(message : string, ...parameters);
  logWarn(message : string, ... parameters);
  logError(message : string, ... parameters);

  getPromiseReject(message : string, ... parameters) : Promise<never>;
  newError(message : string, ... parameters) : ClawizError;

}


export enum ApiRequestMethod {
  GET, POST
}

export interface ApiRequest {

  path                   : string;
  method?                : ApiRequestMethod;
  params?                : any;
  showLoading?           : boolean;
  loadingMessage?        : string;
  synchronizeAfterCall?  : boolean;
  showErrorAlert?        : boolean;
  rejectOnError?         : boolean;

}

export interface ApiHttpResponse {
  status          : string;
  message         : string;
  displayMessage  : string;
  data            : any;
}

export enum ApiErrorType {
  NETWORK_ERROR, SERVER_ERROR, INTERNAL_ERROR
}

export enum ApiProcessPhase {
  PREPARE_CALL, PROCESS_CALL, SYNCHRONIZE_AFTER_CALL
}

export interface ApiResponse {

  request             : ApiRequest;
  success             : boolean;
  processPhase        : ApiProcessPhase;
  errorType           : ApiErrorType;
  message             : string;
  displayMessage      : string;
  logErrorMessage     : string;
  httpResponse        : ApiHttpResponse;

}


export interface ApiService extends Service {

  sessionId        : string;

  get(request : ApiRequest)  : Promise<ApiResponse>;
  post(request : ApiRequest) : Promise<ApiResponse>;

  connect()                  : Promise<ApiResponse>;

  resolveInternalError(message : string, sourceResponse: ApiResponse, reason: any) : Promise<ApiResponse>;
  getNetworkErrorMessage() : string;
}

export interface NodeService extends Service {

  currentUserName : string;
  prepare()       : Promise<any>;

  loginNode() : Promise<ApiResponse>;
  nodeLogged  : boolean;

  loginUser(username : string, password : string) : Promise<ApiResponse>;
  logoutUser() : Promise<ApiResponse>;
  userLogged : boolean;
  changeUserPassword(oldPassword : string, newPassword1 : string, newPassword2 : string) : Promise<ApiResponse>;

}

export interface Loading {

  show() : Promise<any>;
  hide() : Promise<any>

}


export interface YesNoDialogOptions {
  title?       : string;
  message?     : string;
  yesCallback? : () => void;
  noCallback?  : () => void;
  defaultNo?   : boolean;
}

export interface UiService extends Service {

  router:      RouterService;

  openHomePage() : Promise<any>;
  openLoginPage() : Promise<any>;

  showLoading(text? : string) : Promise<Loading>;
  showMessageAlert(title : string, text : string, callback? : () => void) : Promise<any>;
  showErrorAlert(message? : string, callback? : () => void) : Promise<any>;
  showYesNoDialog(options : YesNoDialogOptions) : Promise<any>;

}

export interface RouterService extends Service{

}



export interface StringUtils {

  applyParameters(source : string, ...parameters) : string;

  newGuid() : string;
  capitalize(value : string) : string;
}

export interface DateUtils {

  ISO8601_DATE_FORMAT      : string;
  ISO8601_DATE_TIME_FORMAT : string;

  dateToString(date : Date, format? : string) : string;
  dateTimeToString(date : Date, format? : string) : string ;
  stringToDate(value : string, format? : string) : Date ;
  stringToDateTime(value : string, format? : string) : Date ;

}

export interface TableColumn {
  name      : string;
  type      : string;
}

export interface Table {
  name            : string;
  columns         : TableColumn[];
  createStatement : string;

  addColumn(name : string, type : string) : TableColumn;

}

export interface DataModelField {

  name          : string;
  columnName    : string;

}

export interface DataModel {

  fields : DataModelField[];

  init();

}

export interface DataObject {
  id         : number;

}

export interface QueryCondition {
  filter? : (row : DataObject) => boolean;
}

export enum      QueryOrderDirection {
  ASC, DESC
}

export interface QueryOrder {
  columnName : string;
  direction? : QueryOrderDirection;
}

export interface QueryContext {
  orders?     : QueryOrder[];
  conditions? : QueryCondition[];
  id?         : number;

  addCondition?(condition : QueryCondition);
  addOrder?(order : QueryOrder);

}


export enum DataSourceSubscribeActionType {

  ON_CHANGE_SET_OBJECT_VALUE, ON_CHANGE_SET_ARRAY_VALUE

}

export interface DataSourceSubscribe {

  dataSource   : DataSourceService<any>;
  actionType   : DataSourceSubscribeActionType;
  queryContext : QueryContext;
  object       : any;
  callback     : (obj : any) => Promise<any>;

}

export interface DataSourceService<M extends DataObject> extends Service {
  table                  : Table;
  name                   : string;
  dataSourceApiPath      : string;
  model                  : DataModel;

  prepareObject(object : M) : Promise<any>;

  load(id : number) : Promise<M>;
  loadList(queryContext : QueryContext) : Promise<M[]>;

  save(object : M) : Promise<any>;
  delete(id : number, skipSaveTransactionAction? :boolean) : Promise<any>;

  idToGuid(id : number) : Promise<string>;
  guidToId(guid : string, nullIfNotFound? : boolean, skipServerRequest? : boolean) : Promise<number>;
  serverRowToObject(row : any) : Promise<M>;
  objectToServerRow(object : M) : Promise<any>;

  dump()  : Promise<any>;

  executeSubscribe(subscribe: DataSourceSubscribe) : Promise<any>;
  subscribe(subscribe : DataSourceSubscribe) : Promise<any>;
  unsubscribe(subscribe : DataSourceSubscribe): Promise<any>;

  stopSubscriptions();
  startSubscriptions();

  removeFromIdCache(id : number);

}

export enum DatabaseType {
  SQL, NoSQL
}

export interface DatabaseEngineService extends Service{
  prepare()      : Promise<any>;
  databaseType   : DatabaseType;

  clearDatabase(): Promise<any>;
  executeSql(sql : string, ...parameters) : Promise<any>;

  get(dataSource : DataSourceService<DataObject>, key : string) : Promise<any>;
  set(dataSource : DataSourceService<DataObject>, key : string, value ) : Promise<any>;
  clear(dataSource : DataSourceService<DataObject>) : Promise<any>;
  remove(dataSource : DataSourceService<DataObject>, key : string) : Promise<any>;
  keys(dataSource : DataSourceService<DataObject>): Promise<string[]>;

  createCoreObject(dataSource : DataSourceService<DataObject>, guid : string) : Promise<number>;
  deleteCoreObject(id : number) : Promise<any>;
  saveTransactionAction(dataSource : DataSourceService<DataObject>, objectId : number, action : string) : Promise<any>;

}

/*
export interface StorageConfig {


  init();

}
*/

export interface SynchronizationContext {
  showLoading?   : boolean;
  objectGuid?    : string;
  lastLocalScn?  : number;
  lastRemoteScn? : number;

  rows?           : any[];
}

export interface StorageService extends Service {

  dataSources             : DataSourceService<DataObject>[];

  engine                  : DatabaseEngineService;

  prepare()               : Promise<any>;

  addDataSource<T extends DataSourceService<DataObject>>(c: {new(...args: any[]): T; }, name? : string) : Promise<T> ;
  getDataSource(name : string) : DataSourceService<DataObject>;

  executeSql(sql : string, ...parameters);

  get(dataSource : DataSourceService<DataObject>, key : string) : Promise<any>;
  set(dataSource : DataSourceService<DataObject>, key : string, value ) : Promise<any>;
  clear(dataSource : DataSourceService<DataObject>) : Promise<any>;
  remove(dataSource : DataSourceService<DataObject>, key : string) : Promise<any>;
  keys(dataSource : DataSourceService<DataObject>): Promise<string[]>;

  createCoreObject(dataSource : DataSourceService<DataObject>, guid : string) : Promise<number>;
  deleteCoreObject(id : number) : Promise<any>;
  saveTransactionAction(dataSource : DataSourceService<DataObject>, objectId : number, action : string) : Promise<any>;

  synchronize(params? : SynchronizationContext) : Promise<ApiResponse>;

}

