/*
 * Copyright (c) Clawiz
 */

import {NodeService, ApiResponse, ApiErrorType} from "../interfaces";
import {AbstractService} from "../service/abstract.service";
import {QueryContextImplementation} from "../storage/datasource/query/query.context.implementation";

export class NodeServiceImplementation extends AbstractService implements NodeService {

  public static NODE_KEY        = 'node_key';
  public static NODE_PASSWORD   = 'node_password';
  public static USERNAME        = 'username';

  nodeKey         : string;
  nodePassword    : string;

  currentUserName : string;

  // currentUserId   : number;

  saveNodeConfigParameters() {
    let me     = this;
    let config = me.clawiz.config;

    return config.setParameter(NodeServiceImplementation.NODE_KEY, me.nodeKey)
      .then(() => {
        return config.setParameter(NodeServiceImplementation.NODE_PASSWORD, me.nodePassword);
      })
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception on node initialization : ?', reason.message, reason);
      });

  }

  generateNodeNodeKey() {
    let me     = this;
    let config = me.clawiz.config;
    me.nodeKey         = this.stringUtils.newGuid();
    me.nodePassword    = this.stringUtils.newGuid();

    me.clawiz.logInfo("New node [ key , password ] created : [ ?, ? ]", me.nodeKey, me.nodePassword);

    return me.saveNodeConfigParameters();
  }

  init() : Promise<any> {
    return super.init()
      .then(() => {
        let me     = this;
        let config = me.clawiz.config;

        me.clawiz.logInfo("Node initialization started");

        return config.getParameter(NodeServiceImplementation.NODE_KEY)
          .then((value) => {

            this.clawiz.logInfo('Current node key  = ?', value);
            if ( value != null ) {
              me.nodeKey = value;
              return config.getParameter(NodeServiceImplementation.NODE_PASSWORD)
                .then((value) => {
                  me.nodePassword = value;
                  return Promise.resolve();
                })

            } else {
              return me.generateNodeNodeKey();
            }

          })
          .catch((reason) => {
            return me.clawiz.getPromiseReject('Exception on node initialization : ?', reason.message, reason);
          });
      })

  }

  prepare() : Promise<any> {
    let me = this;
    if ( me.currentUserName == null ) {
      return Promise.resolve();
    }


      return Promise.resolve();
/*
    let ds = me.clawiz.storage.getDataSource('cw_core_users');
    if (ds == null) {
      return me.clawiz.getPromiseReject('Storage cw_core_users not regstered')
    }

    return ds.loadList(new QueryContextImplementation())
      .then((list : any[]) => {

        me.currentUserId     = null;
        for (let row of list) {

          if (row.name === me.currentUsername) {
            me.currentUserId     = row.id;
            return Promise.resolve();
          }
        }

        return Promise.resolve();
      })
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Prepare node current user data failed  : ?', reason.message, reason);
      });
*/
  }

  private _nodeLogged : boolean = false;
  loginNode() : Promise<ApiResponse> {

    this._nodeLogged = false;
    let clawiz       = this.clawiz;
    let me           = this;
    let response     : ApiResponse;

    clawiz.logInfo('Start node login');
    return clawiz.api.post({
      path : '/api/loginNode',
      params : {
        nodename : this.nodeKey,
        password : this.nodePassword
      },
      synchronizeAfterCall : false,
      showLoading          : false
    })
      .then((_response : ApiResponse) => {

        response = _response;

        if ( ! response.success ) {
          if ( response.errorType != ApiErrorType.NETWORK_ERROR ) {
            return Promise.resolve(response);
          }
          clawiz.logInfo('Network connection error. Get last username from config');
          response.success = true;
          return this.clawiz.config.getParameter(NodeServiceImplementation.USERNAME);
        } else {
          this._nodeLogged = true;
          clawiz.logInfo('Node logged successfully with username ?', response['username']);
          return Promise.resolve(response['username']);
        }


      })
      .then((username) => {
        if ( ! response.success ) { return Promise.resolve(response); }
        me.currentUserName = username;
        return me.prepare();
      })
      .then(() => {
        return Promise.resolve(response);
      })
      .catch((reason) => {
        return this.clawiz.api.resolveInternalError('Login node internal error : ?', response, reason);
      });

  }

  get nodeLogged(): boolean {
    return this._nodeLogged;
  }

  loginUser(username: string, password: string) : Promise<ApiResponse>{
    let me       = this;
    let response : ApiResponse;

    this.clawiz.logInfo('Start node login');
    return this.clawiz.api.post({
      path : '/api/loginUser',
      params : {
        username: username,
        password: password

      },
      synchronizeAfterCall : false,
      showLoading          : false,
      showErrorAlert       : false
    })
      .then((_response) => {

        response = _response;

        if ( ! response.success ) {
          let alert : string = null;
          if ( response.errorType == ApiErrorType.NETWORK_ERROR ) {
            alert = this.clawiz.api.getNetworkErrorMessage();
          } else if ( response.message != null && response.message.indexOf('Wrong username') >= 0 ) {
            alert = 'Неверное имя пользователя или пароль';
          }
          this.clawiz.ui.showErrorAlert(alert);
          return Promise.resolve(response);
        }

        me.clawiz.logInfo('User ? logged successfully, response.data.username', username);
        me.currentUserName = username;
        return me.clawiz.config.setParameter(NodeServiceImplementation.USERNAME, username);
      })
      .then(() => {
        return me.prepare();
      })
      .then(() => {
        if ( ! response.success ) { return Promise.resolve(response); }

        return this.clawiz.storage.synchronize()
          .then(() => {
            return this.clawiz.node.prepare();
          })
          .then(() => {
            return this.clawiz.ui.openHomePage()
              .then(() => {
              return Promise.resolve(response);
              });
          });
      })
      .catch((reason) => {
        return this.clawiz.api.resolveInternalError('Login user internal error : ?', response, reason);
      });
  }

  private _clearDatabase(): Promise<any> {
    let me = this;
    return me.clawiz.storage.engine.clearDatabase()
      // .then(() => {
      //   return me.generateNodeNodeKey();
      // })
      .then(() => {
        return me.clawiz.config.setParameter('last_remote_scn', '0')
      })
      .then(() => {
        return me.clawiz.config.setParameter('last_local_scn', '0')
      })
      .catch((reason) => {
        return me.clawiz.getPromiseReject('Exception on rebuild local database : ?', reason.message, reason);
      });
  }

  logoutUser() : Promise<ApiResponse> {
    let response     : ApiResponse;

    this.currentUserName = null;

    return this._clearDatabase()
      .then(() => {
        return this.clawiz.api.post({
          path : '/api/logoutUser',
          synchronizeAfterCall : false,
          showLoading          : false,
          showErrorAlert       : true
        })
      })
      .then((_response) => {
        response = _response;
        if ( ! response.success ) {
          return this.clawiz.logError('Logout user internal error : ?', response.logErrorMessage);
        }
      })
      .then(() => {
        return this.clawiz.config.setParameter(NodeServiceImplementation.USERNAME, null);
      })
      .then(() => {

        return this.clawiz.ui.openLoginPage()
          .then(() => {
            return Promise.resolve(response);
          })

      })
      .catch((reason) => {
        return this.clawiz.api.resolveInternalError('Logout user internal error : ?', response, reason);
      })

  }

  get userLogged(): boolean {
    return this.currentUserName != null;
  }


  changeUserPassword(oldPassword : string, newPassword1 : string, newPassword2 : string): Promise<ApiResponse> {
    return this.clawiz.api.post(
      {
        path : '/api/changeUserPassword',
        params : {
          oldPassword  : oldPassword,
          newPassword1 : newPassword1,
          newPassword2 : newPassword2
        },
        synchronizeAfterCall : false,
        showErrorAlert       : false
      }
    )
      .then((response : ApiResponse) => {
        if ( ! response.success ) {
          if ( response.message.toUpperCase().indexOf('Previous password is wrong'.toUpperCase()) >= 0 ) {
            this.clawiz.ui.showErrorAlert('Неверный пароль. Попробуйте, пожалуйста, еще раз')
          } else {
            this.clawiz.ui.showErrorAlert();
          }
        }
        return Promise.resolve(response);
      })

  }
}
