/*
 * Copyright (c) Clawiz
 */

import {
  Service, Clawiz, StringUtils, ApiService, DateUtils
} from "../interfaces";

export class AbstractService implements Service {

  private _clawiz       : Clawiz;

  private _stringUtils : StringUtils;
  private _dateUtils   : DateUtils;

  get clawiz(): Clawiz {
    return this._clawiz;
  }

  set clawiz(value: Clawiz) {
    this._clawiz = value;
  }

  get stringUtils() : StringUtils {
    if ( this._stringUtils == undefined ) {
      this._stringUtils = this.clawiz.config['_stringUtils'];
    }
    return this._stringUtils;
  }

  get dateUtils() : DateUtils {
    if ( this._dateUtils == undefined ) {
      this._dateUtils = this.clawiz.config['_dateUtils'];
    }
    return this._dateUtils;
  }

  get api() : ApiService {
    return this.clawiz.api;
  }

  protected get className() : string {
    return this.constructor.name;
  }

  protected prepareDataSourceFields() {

  }

  init(): Promise<any> {
    this.prepareDataSourceFields();
    return Promise.resolve(null);
  }
}
