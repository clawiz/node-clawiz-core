/*
 * Copyright (c) Clawiz
 */

import {AbstractService} from "../service/abstract.service";
import {
  ApiService, ApiRequest, ApiResponse, Loading, ApiRequestMethod, ApiProcessPhase,
  ApiErrorType
} from "../interfaces";
import {ApiResponseImplementation} from "./api.response.implementation";
import {ApiRequestImplementation} from "./api.request.implementation";

export class ApiServiceImplementation extends AbstractService implements ApiService {

  sessionId : string;

  public getNetworkErrorMessage() : string {
    return 'Не удалось соединиться с сервером';
  }

  protected getRequestUrl(request : ApiRequest) : string {
    return this.clawiz.config.api.url
      + (request.path.charAt(0) == '/' ? '' : '/')
      + request.path;
  }

  protected showloadingOrNull(request : ApiRequest) : Promise<Loading> {
    if ( request.showLoading ) {
      // return this.clawiz.showLoading('API CALL');
      return this.clawiz.ui.showLoading(request.loadingMessage);
    } else {
      return Promise.resolve(null);
    }
  }

  protected dismissLoading(loading : Loading ) : Promise<any> {
    return loading != null ? loading.hide() : Promise.resolve();
  }

  protected processError(response : ApiResponse) {

    this.clawiz.logError('API request ? ? failed with ? \n params : ?'
      , ApiRequestMethod[response.request.method], response.request.path, response.logErrorMessage, JSON.stringify(response.request.params));
    if ( response.request.showErrorAlert ) { this.clawiz.ui.showErrorAlert(response.displayMessage); }

  }

  protected makeCall(response : ApiResponse, loading : Loading) : Promise<ApiResponse> {
    return Promise.reject('API MAKE CALL METHOD NOT IMPLEMENTED');
  }


  protected createResponse(request : ApiRequest) : ApiResponse {
    let response          = new ApiResponseImplementation();
    response.request      = request;
    response.processPhase = ApiProcessPhase.PREPARE_CALL;
    return response;
  }

  protected processRequest(_request : ApiRequest) : Promise<ApiResponse> {

    const request = new ApiRequestImplementation();
    for (const key of Object.keys(_request) ) {
      request[key] = _request[key];
    }

    let response          = this.createResponse(request);
    response.request      = request;
    response.processPhase = ApiProcessPhase.PREPARE_CALL;

    if ( request.method == null ) {
      response.success      = false;
      response.errorType    = ApiErrorType.INTERNAL_ERROR;
      response.message      = 'Request method not defined';

      this.processError(response);
      return response.request.rejectOnError ? Promise.reject(response) : Promise.resolve(response);
    }

    // this.clawiz.logInfo("call api ? : ?", ApiRequestMethod[request.method], this.getRequestUrl(request));

    let loading = null;

    return this.showloadingOrNull(request)
      .then((_loading) => {

        loading = _loading;

        response.processPhase = ApiProcessPhase.PROCESS_CALL;
        return this.makeCall(response, loading);

      })
      .then((_response : ApiResponse) => {
        response = _response;
        if ( response.success ) {
          return Promise.resolve(response);
        }

        if ( response.message != null
          && ( response.message.toUpperCase().indexOf('Wrong session id'.toUpperCase())                        >= 0
            || response.message.toUpperCase().indexOf("Parameter 'cwsessionid' must be defined".toUpperCase()) >= 0
          )
        ) {
          this.sessionId = null;
          return this.connect()
            .then((connectResponse: ApiResponse) => {
              if ( connectResponse.success) {
                return this.clawiz.node.loginNode();
              } else {
                return Promise.resolve(connectResponse);
              }
            })
            .then((loginResponse : ApiResponse) => {
              if ( loginResponse.success) {
                response = this.createResponse(request);
                return this.makeCall(response, loading)
                  .then((recallResponse : ApiResponse) => {
                    response = recallResponse;
                    return Promise.resolve(response);
                  });
              } else {
                return Promise.resolve(response);
              }
            });
        } else {
          return Promise.resolve(response);
        }

      })
          .then(() => {

            // skip this step on network failure
            if ( ! response.success ) {
              return Promise.resolve(response);
            }

            response.processPhase = ApiProcessPhase.SYNCHRONIZE_AFTER_CALL;

            if ( request.synchronizeAfterCall ) {
              return this.clawiz.storage.synchronize({
                showLoading : false
              })
                .then((synchronizeResponse : ApiResponse) => {
                  if ( synchronizeResponse.success) {
                    return Promise.resolve(response);
                  } else {
                    response.success      = false;
                    response.errorType    = synchronizeResponse.errorType;
                    response.message      = synchronizeResponse.logErrorMessage;
                    return Promise.resolve(response);
                  }
                });
            } else {
              return Promise.resolve(response);
            }
          })
          .then(() => {
            return this.dismissLoading(loading)
              .then(() => {
                return Promise.resolve(response);
              });
          })
          .then(() => {

            if ( ! response.success ) {
              if ( response.errorType === null ) {
                response.errorType = ApiErrorType.SERVER_ERROR;
              }
              this.processError(response);
              if ( response.request.rejectOnError ) {
                return Promise.reject(response);
              }
            }

            return Promise.resolve(response);
          })
          .catch( (reason) => {

            return this.dismissLoading(loading)
              .then(() => {
                // if we catch our reject then escalate it
                if ( reason.request != null ) {
                  return Promise.reject(response);
                }

                if ( loading != null ) { loading.dismiss(); }
                response.success      = false;
                response.errorType    = ApiErrorType.INTERNAL_ERROR;
                response.processPhase = ApiProcessPhase.PROCESS_CALL;
                response.message = reason.message;

                this.processError(response);

                return response.request.rejectOnError ? Promise.reject(response) : Promise.resolve(response);

              });

          });



  }

  get(request : ApiRequest) : Promise<ApiResponse> {
    request.method = ApiRequestMethod.GET;
    return this.processRequest(request);
  }

  post(request : ApiRequest) : Promise<ApiResponse> {
    request.method = ApiRequestMethod.POST;
    return this.processRequest(request);
  }

  connect() : Promise<ApiResponse> {

    this.sessionId = null;

    return this.post({
      path                 : '/api/connect',
      synchronizeAfterCall : false,
      showLoading          : false
    })
      .then((response : ApiResponse) => {
        if ( response.success ) {
          this.sessionId = response['cwsessionid'];
        } else {
          this.clawiz.logError('Connect to server failed  with ?', response.logErrorMessage);
        }
        return Promise.resolve(response);
      });
  }


  resolveInternalError(message : string, sourceResponse: ApiResponse, reason: any) : Promise<ApiResponse>{
    this.clawiz.logError(message, reason.message, reason);
    const response = new ApiResponseImplementation();
    if ( sourceResponse != null ) {
      for (const key of Object.keys(sourceResponse)) {
        response[key] = sourceResponse[key];
      }
    }
    response.success   = false;
    response.errorType = ApiErrorType.INTERNAL_ERROR;
    response.message   = this.stringUtils.applyParameters(message, response.message);
    return Promise.resolve(response);
  }

}
