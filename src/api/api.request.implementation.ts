/*
 * Copyright (c) Clawiz
 */

import {ApiRequest, ApiRequestMethod} from "../interfaces";

export class ApiRequestImplementation implements ApiRequest {


  method                : ApiRequestMethod;
  path                  : string;
  params?               = {};

  synchronizeAfterCall  : boolean = true;
  showLoading           : boolean = true;
  loadingMessage        : string;
  showErrorAlert        : boolean = true;
  rejectOnError         : boolean = false;

}


