/*
 * Copyright (c) Clawiz
 */

import {ApiRequest, ApiResponse, ApiErrorType, ApiProcessPhase, ApiHttpResponse} from "../interfaces";

export class ApiResponseImplementation implements ApiResponse {

  request             : ApiRequest;
  success             : boolean = true;
  processPhase        : ApiProcessPhase;
  errorType           : ApiErrorType;
  message             : string;
  displayMessage      : string;

  httpResponse        : ApiHttpResponse;

  set logErrorMessage(message : string) {

  }

  get logErrorMessage(): string {
    return (this.errorType != null ? ApiErrorType[this.errorType] : '')
      + (this.processPhase   != null ? ' ' + ApiProcessPhase[this.processPhase] : '')
      + (this.message        != null ? ' ' + this.message : '')
      + (this.displayMessage != null ? ' ' + this.displayMessage : '');
  }
}
