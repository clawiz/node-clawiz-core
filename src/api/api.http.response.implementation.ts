

/*
 * Copyright (c) Clawiz
 */

import {ApiHttpResponse} from "../interfaces";
export class ApiHttpResponseImplementation implements ApiHttpResponse {

  status          : string;
  message         : string;
  displayMessage  : string;
  data            : any;
}
