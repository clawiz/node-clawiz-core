

/*
 * Copyright (c) Clawiz
 */

import {ClawizConfig, Clawiz} from '../interfaces';
import {ClawizImplementation } from "./clawiz.implementation";
import {ClawizConfigImplementation} from "./clawiz.config.implementation";

export class ClawizBuilder {

  private static _clawiz : Clawiz;

  private static _ready  : boolean = false;

  public static get clawiz() : Clawiz {
    return this._clawiz;
  }

  public static get ready() : boolean {
    return this._ready;
  }

  protected static createClawizInstance<T extends ClawizImplementation, C extends ClawizConfigImplementation> (
    clawizClass : { new (...args: any[]): T },
    configClass : { new (...args: any[]): C }
    , _config : ClawizConfig
  ) : Promise<T> {

    let config    = new configClass();
    let clawiz    = new clawizClass();

    for ( let key in _config ) {
      config[key] = _config[key];
    }

    config['clawiz'] = clawiz;
    clawiz.config    = config;

    ClawizBuilder._clawiz = clawiz;
    return clawiz.init()
      .then(() => {
        ClawizBuilder._ready = true;
        return Promise.resolve(clawiz);
      });

  }


  public static init(config : ClawizConfig) : Promise<Clawiz> {
    return ClawizBuilder.createClawizInstance(ClawizImplementation, ClawizConfigImplementation, config)
      .then((clawiz : Clawiz) => {
        this._clawiz = clawiz;
        return Promise.resolve(clawiz);
      });
  }

}

