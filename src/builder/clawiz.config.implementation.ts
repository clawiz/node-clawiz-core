
/*
 * Copyright (c) Clawiz
 */

import {
  ClawizConfig, Clawiz, DataSourceService, DataObject, ApplicationConfig, ApiConfig,
  StorageConfig, UiConfig
} from "../interfaces";

export class ClawizConfigImplementation implements ClawizConfig {

  clawiz                  : Clawiz;
  application             : ApplicationConfig;
  storage                 : StorageConfig;
  api                     : ApiConfig;
  ui                      : UiConfig;
  locale                  : string = 'ru';

  private _configDatasource : DataSourceService<DataObject>;
  private get configDatasource() {
    if ( this._configDatasource == null ) {
      this._configDatasource = this.clawiz.storage.getDataSource('cw_core_config_parameters');
    }
    return this._configDatasource;
  }



  getParameter(name : string) {
    if ( name == null ) {
      return this.clawiz.getPromiseReject('Configuration parameter name cannot be null');
    }

    return this.clawiz.storage.get(this.configDatasource, name)
      .then((row) => {
        return Promise.resolve(row != null ? row.value : null);
      });

  }


  getNumberParameter(name: string) {
    let me = this;
    return this.getParameter(name)
      .then((value) => {
        let result = Number(value);
        if ( result != NaN ) {
          return Promise.resolve(result);
        } else {
          return me.clawiz.getPromiseReject('Wrong config parameter ? number format ?', name, value)
        }
      })
      .catch((reason) => {
        return Promise.reject(reason);
      });
  }

  setParameter(name : string, value : string) {
    if ( name == null ) {
      return this.clawiz.getPromiseReject('Configuration parameter name cannot be null');
    }

    return this.clawiz.storage.set(this.configDatasource, name
      ,{
        name : name,
        value : value
      }
    );


  }


}
