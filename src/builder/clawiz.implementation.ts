
/*
 * Copyright (c) Clawiz
 */

import {
    ClawizConfig, Clawiz, Service, ApiService, Loading, ClawizError, StringUtils, UiService,
    StorageService, NodeService, ApiResponse, DateUtils, RouterService
} from "../interfaces";
import {ApiServiceImplementation} from "../api/api.service.implementation";
import {StringUtilsImplementation} from "../utils/string.utils.implementation";
import {UiServiceImplementation} from "../ui/ui.service.implementation";
import {StorageServiceImplementation} from "../storage/storage.service.implementation";
import {NodeServiceImplementation} from "../node/node.service.implementation";
import {ClawizErrorImplementation} from "../error/clawiz.error.implementation";
import {DateUtilsImplementation} from "../utils/date.utils.implementation";
import {AbstractRouterService} from "../ui/router/abstract.router.service";

export class ClawizImplementation implements Clawiz {

  protected _config   : ClawizConfig;
  protected _api      : ApiService;
  protected _ui       : UiService;
  protected _storage  : StorageService;
  protected _node     : NodeService;

  get config(): ClawizConfig {
    return this._config;
  }

  set config(value: ClawizConfig) {
    this._config = value;
  }


  get api(): ApiService {
    return this._api;
  }

  set api(value: ApiService) {
    this._api = value;
  }

  get ui(): UiService {
    return this._ui;
  }

  set ui(value: UiService) {
    this._ui = value;
  }

  get storage(): StorageService {
    return this._storage;
  }

  set storage(value: StorageService) {
    this._storage = value;
  }

  get node(): NodeService {
    return this._node;
  }

  set node(value: NodeService) {
    this._node = value;
  }


  private stringUtils : StringUtils;
  private dateUtils   : DateUtils;

  protected getStorageService() : Promise<StorageServiceImplementation> {
    return this.getService(StorageServiceImplementation);
  }

  protected getApiService() : Promise<ApiServiceImplementation> {
    return this.getService(ApiServiceImplementation);
  }

  protected getNodeService() : Promise<NodeServiceImplementation> {
    return this.getService(NodeServiceImplementation);
  }

  protected getUiService() : Promise<UiServiceImplementation> {
    return this.getService(UiServiceImplementation);
  }

  protected getRouterService() : Promise<AbstractRouterService> {
    return this.getService(AbstractRouterService);
  }

  protected createServices() : Promise<any>{
    return this.getService(StringUtilsImplementation)
      .then((service : StringUtils) => {
        this.config['_stringUtils'] = service;
        this.stringUtils            = service;
        return this.getService(DateUtilsImplementation)
      })
      .then((service : DateUtils) => {
        this.config['_dateUtils'] = service;
        this.dateUtils            = service;
        return this.getStorageService();
      })
      .then((service : StorageService) => {
        this.storage = service;
        return this.storage.prepare()
      })
      .then(() => {
        return this.getApiService()
      })
      .then((service : ApiServiceImplementation) => {
        this.api = service;
        return this.getUiService() } )
      .then((service : UiServiceImplementation) => {
        this.ui  = service;
        return this.getNodeService()
      })
      .then((service : NodeServiceImplementation) => {
        this.node = service;
        return this.getRouterService();
      })
      .then(( service : RouterService) => {
        this.ui.router = service;
        // this.ui.router = service;
        return Promise.resolve();
      });

  }

  init(): Promise<any> {
    return this.createServices()
      .then(() => {
        return this.api.connect()
      })
      .then((response : ApiResponse) =>  {
        if ( response.success) {
          return this.node.loginNode()
        } else {
          return Promise.resolve(response);
        }
      })
      .then((response : ApiResponse) => {
        if ( response.success && this.node.userLogged ) {
          return this.storage.synchronize();
        } else {
          return Promise.resolve(response);
        }
      })
      .then((response : ApiResponse) => {
        if ( response.success ) {
          this.logInfo("Clawiz core initialized");
        } else {
          return Promise.reject(response);
        }
      })
      .catch((reason) => {
        return this.getPromiseReject('Clawiz core initialization error : ?', reason.message, reason);
      });
  }

  getService<T extends Service>(c: {new(...args: any[]): T; }) : Promise<T> {
    let service = new c();
    service.clawiz = this;
    return service.init()
      .then(() => {
        return Promise.resolve(service)
      });
  }

  private processMessageParameters(message:string, ...parameters) : string {
    return this.stringUtils.applyParameters(message, parameters);
  }

  logInfo(message:string, ...parameters) {
    console.info(this.processMessageParameters(message, parameters));
  }

  logWarn(message:string, ...parameters) {
    console.warn(this.processMessageParameters(message, parameters));
  }

  logError(message:string, ...parameters) {
    console.error(this.processMessageParameters(message, parameters));
  }

  newError(message : string, ... parameters) : ClawizError {
    this.logError(message, parameters);
    let error = new ClawizErrorImplementation(this);

    error.message    = this.stringUtils.applyParameters(message, parameters);
    error.parameters = parameters;

    return error;
  }

  getPromiseReject(message : string, ... parameters) {
    return Promise.reject(this.newError(message, parameters));
  }
}

