/*
 * Copyright (c) Clawiz
 */

import {AbstractUiComponent} from "../abstract.ui.component";
import {Loading, ApiResponse} from "../../../interfaces";
import {FormModel} from "./form.model";

export class PrepareInputContext {
  id   : string;
  name : string;
}

export class PrepareDateInputContext extends PrepareInputContext {

}

export class SelectValue {
  value : string;
  text  : string;
}

export class QuerySelectValuesRequest {
  name     : string;
  pattern? : string;
}

export class QuerySelectValuesResponse {
  success     : boolean;
  request     : QuerySelectValuesRequest;
  values      : SelectValue[];

  apiResponse : ApiResponse;

}


export class PrepareStaticSelectInputContext extends PrepareInputContext {
  options : SelectValue[];
}

export class PrepareDynamicSelectInputContext extends PrepareInputContext {
}

export class ValidateFormDataError {

  elementId : string;
  message   : string;

}

export class ValidateFormDataContext {

  errors : ValidateFormDataError[];

  addError(error : ValidateFormDataError ) {
    this.errors.push(error);
  }

  get isValid() : boolean {
    return this.errors.length == 0;
  }

}


export class AbstractFormComponent<M extends FormModel> extends AbstractUiComponent {

  private _apiPath          : string;
  private _data             : M = <M> {};
  private _id               : string;
  private _action           : string;
  private _parentLinkValues : any;

  private _locked           : boolean = false;

  get apiPath(): string {
    return this._apiPath;
  }

  set apiPath(value: string) {
    this._apiPath = value;
  }


  get data(): M {
    return this._data;
  }

  set data(value: M) {
    this._data = value;
  }


  get locked(): boolean {
    return this._locked;
  }

  public lock() {
    this._locked = true;
  }

  public unlock() {
    this._locked = false;
  }


  get saveButtonText() {
    return "Сохранить";
  }

  get discardButtonText() {
    return "Отменить";
  }


  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }


  get action(): string {
    return this._action;
  }

  set action(value: string) {
    this._action = value;
  }

  get parentLinkValues(): any {
    return this._parentLinkValues;
  }

  set parentLinkValues(value: any) {
    this._parentLinkValues = value;
  }

  dateToString(date : Date) {
    return date != null ? date.toISOString() : null;
  }

  stringToDate(string : string) : Date {
    return string != null ? new Date(Date.parse(string)) : null;
  }

  createModelInstance() : M {
    return null;
  }

  processLoadResponse(response : ApiResponse) : Promise<any> {

    let _data : M = this.createModelInstance();

    for( let key in response['data'] ) {
      _data[key] = response['data'][key];
    }

    if ( _data.id != null ) {
      this.id = _data.id;
    }

    return this.processLoadData(_data)
      .then(() => {
        this.data = _data;
        return Promise.resolve();
      });

  }

  processLoadData(data : M) : Promise<any> {
    return Promise.resolve();
  }

  protected load() : Promise<any> {


    this.lock();

    let apiAction : string;
    if ( this.action == 'edit' ) {
      apiAction = 'load';
    } else if ( this.action == 'create' ) {
      apiAction = 'create';
    } else {
      return this.clawiz.getPromiseReject('Wrong form call action ?', this.action);
    }



    return this.clawiz.api.post({
      path                 : this.apiPath,
      params               : {
        action : apiAction,
        id     : this.id
      },
      synchronizeAfterCall : false
    })
      .then((response : ApiResponse) => {
        if ( response.success ) {
          return this.processLoadResponse(response)
            .then(() => {
              this.unlock();
              return Promise.resolve();
            });
        }
        this.unlock();
        return Promise.resolve();
      })
        .catch( (response : any ) => {
            this.unlock();
            return Promise.resolve();
        } );

  }

  prepareSavedData(data : M) : Promise<any> {
    data.id = this.data.id;
    return Promise.resolve();
  }

  validateFormData() : Promise<ValidateFormDataContext> {
    let context = new ValidateFormDataContext();
    context.errors = [];
    return Promise.resolve(context);
  }

  displayValidateErrors(validateContext : ValidateFormDataContext ) {
    for (let error of validateContext.errors ) {
      this.clawiz.ui.showErrorAlert(error.message);
    }
  }

  save(params : any = {}) : Promise<any> {

    this.lock();

    let closeAfterSave = params.closeAfterSave != null ? params.closeAfterSave : true;

    let apiParameters : any = {
      action : 'save',
      id     : this.id
    };

    if ( params.api != null ) {
      for (let key in params.api) {
        apiParameters[key] = params.api[key];
      }
    }

    let _data : any = {};

    return this.validateFormData()
      .then((validateContext : ValidateFormDataContext) => {
        return this.prepareSavedData(_data)
          .then(() => {

            if ( ! validateContext.isValid ) {
              this.displayValidateErrors(validateContext);
              return Promise.reject(null);
            }

            if ( this.parentLinkValues != null ) {
              for( let key in this.parentLinkValues ) {
                _data[key] = this.parentLinkValues[key];
              }
            }

            apiParameters.model  = JSON.stringify(_data);

            return this.clawiz.api.post({
              path                 : this.apiPath,
              params               : apiParameters,
              synchronizeAfterCall : false
            })

          })
      })
      .then((response : ApiResponse) => {
        if ( response.success ) {
          if ( closeAfterSave ) {
            return this.afterSaveClose()
              .then(() => {
                this.unlock();
                return Promise.resolve();
              });
          } else {
            return this.processLoadResponse(response)
              .then(() => {
                this.unlock();
                return Promise.resolve();
              });
          }
        }
        this.unlock();
        return Promise.resolve();
      })
        .catch( (response : any ) => {
            this.unlock();
            return Promise.resolve();
        } );
  }

  afterSaveClose() : Promise<any> {
    return Promise.resolve();
  }

  discard() : Promise<any> {

    this.clawiz.ui.showYesNoDialog({
      title: 'Закрыть ?',
      message: 'Отменить изменения ?',
      defaultNo: true,
      yesCallback: this.afterDiscardClose.bind(this)
    });


    return Promise.resolve();
  }

  afterDiscardClose() : Promise<any> {
    return this.afterSaveClose();
  }

  protected beforeOpen(): Promise<any> {
    return super.beforeOpen()
      .then(() => {
        return this.load();
      });
  }

  protected prepareDateInput(context : PrepareDateInputContext) : Promise<any> {
    return Promise.resolve();
  }

  protected prepareStaticSelectInput(context : PrepareStaticSelectInputContext) : Promise<any> {
    return Promise.resolve();
  }

  protected querySelectValues(request : QuerySelectValuesRequest) : Promise<QuerySelectValuesResponse> {

    let params : any = {
      action : 'querySelectValues',
      name   : request.name
    };

    if ( request.pattern ) {
      params.pattern = request.pattern;
    }

    return this.clawiz.api.post({
      path                 : this.apiPath,
      params               : params,
      synchronizeAfterCall : false
    })
      .then((response : ApiResponse) => {

        let result : QuerySelectValuesResponse = {
          success     : response.success,
          apiResponse : response,
          request     : request,
          values      : response['selectValues']
        };

        return Promise.resolve(result);
      });
/*
      .then(() => {
        return Promise.resolve();
      });
*/

  }

  protected prepareDynamicSelectInput(context : PrepareDynamicSelectInputContext) : Promise<any> {
    return Promise.resolve();
  }

}

