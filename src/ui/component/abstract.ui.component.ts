/*
 * Copyright (c) Clawiz
 */

import {AbstractService} from "../../service/abstract.service";
import {
  ApiResponse, DataSourceSubscribeActionType, QueryOrder, QueryCondition, DataObject,
  DataSourceService, Loading
} from "../../interfaces";
import {DataSourceSubscribeImplementation} from "../../storage/datasource/data.source.subsribe.implementation";

export class SubscribedObject {
  id                 : number;
  dataSource         : DataSourceService<DataObject>;
}

export class AbstractUiComponent extends AbstractService {

  private _subscribes                    : DataSourceSubscribeImplementation[] = [];
  private _subscribedObjects             : SubscribedObject[] = [];
  private _autoSynchronizationInterval  = 0;
  private _inPrepare                    = false;
  private _controlsEnabled               = true;


  get subscribes(): DataSourceSubscribeImplementation[] {
    return this._subscribes;
  }

  get subscribedObjects(): SubscribedObject[] {
    return this._subscribedObjects;
  }

  get inPrepare() : boolean {
    return this._inPrepare;
  }


  get controlsEnabled(): boolean {
    return this._controlsEnabled;
  }

  set controlsEnabled(value: boolean) {
    this._controlsEnabled = value;
  }

  protected set autoSynchronizationInterval(interval : number) {
    this._autoSynchronizationInterval = interval;
  }

  protected get autoSynchronizationInterval() {
    return this._autoSynchronizationInterval;
  }

  protected disableAutoSynchronization() {
    this.autoSynchronizationInterval = 0;
  }

  protected get autoSynchronizationEnabled() : boolean {
    return this.autoSynchronizationInterval > 0;
  }


  protected subscribeObject(callback : (obj : any) => any, dataSource : DataSourceService<DataObject>, id : number) : any {

    let subscribe = new DataSourceSubscribeImplementation();

    subscribe.actionType = DataSourceSubscribeActionType.ON_CHANGE_SET_OBJECT_VALUE;
    subscribe.object     = this;
    subscribe.callback   = callback.bind(this);
    subscribe.dataSource = dataSource;

    subscribe.queryContext.id = id;

    this._subscribes.push(subscribe);

    this._subscribedObjects.push({
      id         : id,
      dataSource : dataSource
    });

    if ( ! this._inPrepare ) {
      return dataSource.subscribe(subscribe);
    } else {
      return subscribe;
    }
  }

  private subscribeArrayReceiver(dataSource : DataSourceService<any>, data : any[]) {
    if ( data == null ) {
      return;
    }
    for ( let obj of data ) {
      this._subscribedObjects.push({
        id         : obj.id,
        dataSource : dataSource
      })

    }
  }

  protected subscribeArray(callback : (obj : any) => any, dataSource : DataSourceService<DataObject>, conditions? : QueryCondition[], orders? : QueryOrder[]) : any {

    let subscribe = new DataSourceSubscribeImplementation();

    subscribe.actionType = DataSourceSubscribeActionType.ON_CHANGE_SET_ARRAY_VALUE;
    subscribe.object     = this;
    subscribe.callback   = callback.bind(this);

    if ( conditions != null ) {
      for ( let condition of conditions ) {
        condition.filter = condition.filter.bind(this);
        subscribe.queryContext.addCondition(condition);
      }
    }

    if ( orders != null ) {
      for ( let order of orders) {
        subscribe.queryContext.addOrder(order);
      }
    }

    subscribe.dataSource = dataSource;
    this._subscribes.push(subscribe);

    let internalSubscribe          = new DataSourceSubscribeImplementation();
    internalSubscribe.dataSource   = subscribe.dataSource;
    internalSubscribe.actionType   = subscribe.actionType;
    internalSubscribe.object       = this;
    internalSubscribe.queryContext = subscribe.queryContext;
    internalSubscribe.callback     = function( obj: any) : Promise<any> {
      this.subscribeArrayReceiver(dataSource, obj);
      return Promise.resolve();
    }.bind(this);

    this._subscribes.push(internalSubscribe);

    if ( ! this._inPrepare ) {

      return dataSource.subscribe(subscribe)
        .then(() => {
          return dataSource.subscribe(internalSubscribe);
        })

    } else {
      return Promise.resolve(subscribe);
    }

  }

  protected showErrorAlert(text : string) {
    this.clawiz.ui.showErrorAlert(text);
  }


  protected reloadSubscribes() : Promise<any> {
    let promises : Promise<any>[] = [];
    for(let s of this._subscribes ) {
      promises.push(s.dataSource.executeSubscribe(s));
    }
    return Promise.all(promises);
  }

  protected subscribeAll() : Promise<any>{
    let promises : Promise<any>[] = [];
    for ( let s of this._subscribes ) {
      promises.push(s.dataSource.subscribe(s));
    }
    return Promise.all(promises);
  }

  protected unsubscribeAll() : Promise<any> {
    this._subscribedObjects = [];

    let promises : Promise<any>[] = [];
    for ( let s of this._subscribes ) {
      promises.push(s.dataSource.unsubscribe(s));
    }
    this._subscribes = [];
    return Promise.all(promises);
  }

  protected synchronize() : Promise<ApiResponse> {
    return this.clawiz.storage.synchronize();
  }

  private _synchronizationIntervalId : number = -1;

  protected startSynchronizationInterval() : Promise<any> {
    if ( ! this.autoSynchronizationEnabled ) {
      return Promise.resolve();
    }

    let me = this;

    if ( this._synchronizationIntervalId == -1 ) {

      this._synchronizationIntervalId = window.setInterval(
        function (){
          me.clawiz.storage.synchronize({
            showLoading : false
          });
        }
        ,this.autoSynchronizationInterval);
    }

    return me.clawiz.storage.synchronize({
      showLoading : false
    });
  }

  protected stopSynchronizationInterval() : Promise<any> {
    if ( this._synchronizationIntervalId != -1 ) {
      window.clearInterval(this._synchronizationIntervalId);
      this._synchronizationIntervalId = -1;
    }
    return Promise.resolve();
  }

  protected prepare(): Promise<any> {
    return Promise.resolve();
  }

  protected beforeOpen() : Promise<any> {
    this._inPrepare = true;
    this.prepareDataSourceFields();
    return this.startSynchronizationInterval()
      .then(() => {
        this.prepare()
          .then(() => {
            this._inPrepare = false;
            return this.subscribeAll();
          })
          .catch((reason) => {
            this._inPrepare = false;
            return Promise.reject(reason);
          });
      })
  }

  protected beforeClose() : Promise<any> {
    return this.stopSynchronizationInterval()
      .then(() => {
        this.unsubscribeAll();
      })
      .catch((reason) => {
        console.error('Before close exception ' + JSON.stringify(reason));
      })
  }



}

