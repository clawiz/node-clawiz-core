/*
 * Copyright (c) Clawiz
 */

import {AbstractUiComponent} from "../abstract.ui.component";
import {ApiResponse} from "../../../interfaces";
import {GridRowModel} from "./grid.row.model";
import {GridLink} from "./grid.link";

export abstract class AbstractGridComponent<M extends GridRowModel> extends AbstractUiComponent {


  private _apiPath               : string;
  private _editFormName          : string;
  private _inlineEdit            : boolean = false;
  private _totalRowsCount        : number;
  private _rowsPerPage           : number   = 10;
  private _pagesBufferSize       : number   = 3;
  private _currentPage           : number   = 1;
  private _pageSelectedRowIndex  : number   = null;
  private _inlineEditRowIndex    : number   = null;
  private _allRowsFetched        : boolean;

  private _parentLinks           : GridLink[] = [];
  private _childLinks            : GridLink[] = [];
  private _autoLoadEnabled       : boolean = true;

  private _rowsBuffer            : M[];
  private _rowsDisplayBuffer     : M[] = [];
  private _displayFirstRowIndex  : number;

  private _locked                : boolean = false;

  private _loadToken             : string;

  get apiPath(): string {
    return this._apiPath;
  }

  set apiPath(value: string) {
    this._apiPath = value;
  }


  get editFormName(): string {
    return this._editFormName;
  }

  set editFormName(value: string) {
    this._editFormName = value;
  }


  get inlineEdit(): boolean {
    return this._inlineEdit;
  }

  set inlineEdit(value: boolean) {
    this._inlineEdit = value;
  }

  get createRowButtonText() {
    return "Создать";
  }

  get editRowButtonText() {
    return "Изменить";
  }

  get deleteRowButtonText() {
    return "Удалить";
  }

  get totalRowsCount(): number {
    return this._totalRowsCount;
  }

  set totalRowsCount(value: number) {
    this._totalRowsCount = value;
  }

  get allRowsFetched(): boolean {
    return this._allRowsFetched;
  }

  set allRowsFetched(value: boolean) {
    this._allRowsFetched = value;
  }


  get locked(): boolean {
    return this._locked;
  }

  public lock() {
    this._locked = true;
  }

  public unlock() {
    this._locked = false;
  }

  get pageSelectedRowIndex(): number {
    return this._pageSelectedRowIndex;
  }


  get inlineEditRowIndex(): number {
    return this._inlineEditRowIndex;
  }

  get rowsDisplayBuffer(): M[] {
    return this._rowsDisplayBuffer;
  }

  set rowsDisplayBuffer(value:  M[]) {
    this._rowsDisplayBuffer = value;
  }

  get rowsPerPage(): number {
    return this._rowsPerPage;
  }

  set rowsPerPage(value: number) {
    this._rowsPerPage = value;
  }


  get pagesBufferSize(): number {
    return this._pagesBufferSize;
  }

  set pagesBufferSize(value: number) {
    this._pagesBufferSize = value;
  }

  get autoLoadEnabled(): boolean {
    return this._autoLoadEnabled;
  }

  public enableAutoLoad() {
    this._autoLoadEnabled = true;
  }

  public disableAutoLoad() {
    this._autoLoadEnabled = false;
  }


  get loadToken(): string {
    return this._loadToken;
  }

  sort() : Promise<any> {
    return Promise.resolve();
  }

  private _makeSetParentLink(link : GridLink) {
    for(let check of this._parentLinks ) {
      if ( check.name == link.name ) {
        check.values = link.values;
        return;
      }
    }
    this._parentLinks[this._parentLinks.length] = link;

  }

  public setParentLink(link : GridLink) : Promise<any> {
    this._makeSetParentLink(link);

    return this._reload();
  }

  private _makeSetChildLink(link : GridLink) {
    for(let check of this._childLinks ) {
      if ( check.name == link.name ) {
        check.values = link.values;
      }
    }
    this._childLinks[this._childLinks.length] = link;
  }

  public setChildLink(link : GridLink) {
    this._makeSetChildLink(link);
  }

  afterLoadRow(row : M) : Promise<any> {
    return Promise.resolve();
  }

  protected get loadParentFilters() : any {

    if ( this._parentLinks.length == 0 ) {
      return null;
    }

    let filter : any = {};
    filter.conditions = [];


    for(let link of this._parentLinks ) {
      for ( let value of link.values ) {
        filter.conditions[filter.conditions.length] = {
          columnName : value.columnName
          ,operator  : 'equal'
        }
      }
    }

    return filter;
  }

  protected get loadParentFilterParameters() : any {
    let values = [];
    for(let link of this._parentLinks ) {
      for ( let value of link.values ) {
        values[values.length] = value.value;
      }
    }
    return values;
  }

  protected get createParentParameters() : any {
    let param : any = {};

    for ( let link of this._parentLinks ) {
      for ( let value of link.values ) {
        param[value.columnName] = value.value;
      }
    }

    return param;
  }

  createRowModelInstance() : M {
    return null;
  }

  protected load(context : any) : Promise<any> {

    if ( this.loadToken == null ) {
      this._loadToken = this.stringUtils.newGuid();
    }

    return this.clawiz.api.post({
      path                 : this.apiPath,
      params               : {
        action        : 'load',
        loadToken     : this.loadToken,
        startRow      : context.startRow,
        endRow        : context.endRow,
        filter        : JSON.stringify(this.loadParentFilters),
        filterValues  : JSON.stringify(this.loadParentFilterParameters)
      },
      synchronizeAfterCall : false
    })
      .then((response : ApiResponse) => {
        if ( response.success ) {

          let model    = response['model'];

          let promises : Promise<any>[] = [];

          for (let key in model) {
            if ( key == 'data') {

              let data = model.data;

              for(let i=0; i < data.length; i++ ) {
                let row : any = this.createRowModelInstance();
                this._rowsBuffer[i+context.startRow] = row;
                let modelRow = data[i];
                for ( let rk in modelRow ) {
                  row[rk] = modelRow[rk];
                }
                promises.push(this.afterLoadRow(row));
              }

            } else {
              this[key] = model[key];
            }

          }
          this.totalRowsCount = this._rowsBuffer.length - 1;
          return Promise.all(promises);
        }
      });

  }


  get displayFirstRowIndex(): number {
    return this._displayFirstRowIndex;
  }

  protected _clearCaches() {
    this._rowsBuffer     = [];
    this._rowsBuffer[0]  = <M>{id : null};
    this._displayFirstRowIndex  = -1;
    this._totalRowsCount = -1;
    this._allRowsFetched = false;
  }

  public _reload(fistRowIndex? : number) : Promise<any> {
    this._clearCaches();
    return this.setDisplayFirstRowIndex(fistRowIndex == null ? 1 : fistRowIndex)
      .then(() => {
        if ( this._childLinks.length > 0 ) {
          if ( this._rowsDisplayBuffer.length > 1) {
            return this.selectRow(0, null);
          }
        }
      });
  }

  protected beforeOpen(): Promise<any> {
    return super.beforeOpen()
      .then(() => {
        this._clearCaches();
        if ( this.autoLoadEnabled ) {
          return this._reload();
        } else {
          return Promise.resolve();
        }
      });
  }

  protected beforeClose() : Promise<any> {

    console.log('CLOSE COMPOMENT' + this.apiPath);

    return this.clawiz.api.post({
      path                 : this.apiPath,
      params               : {
        action    : 'close',
        loadToken : this.loadToken
      },
      synchronizeAfterCall : false
    });

  }

  private _refreshRows(index : number) : Promise<any> {

    if ( index < 1 ) {
      return this.clawiz.getPromiseReject('Cannot refresh rows for 0 index');
    }

    this._displayFirstRowIndex = index;
    let _rows : M[] = [];

    for (let i = 0; i < this.rowsPerPage && (i + this._displayFirstRowIndex ) < this._rowsBuffer.length; i++) {
      _rows.push(this._rowsBuffer[i+index]);
    }

    this.rowsDisplayBuffer         = _rows;
    this._currentPage = Math.trunc((this._displayFirstRowIndex - 1) / this._rowsPerPage + 1);

    this.unlock();

    return Promise.resolve();
  }


  public setDisplayFirstRowIndex(_index : number) : Promise<any> {
    if ( this.locked ) { return; }

    let index = _index;

    if ( index < 1 ) {
      index = 1;
    }
    if ( this.allRowsFetched ) {
      if ( ( index + this.rowsPerPage ) >= this._rowsBuffer.length ) {
        index = this._rowsBuffer.length - this.rowsPerPage;
        if ( index < 1 ) {
          index = 1;
        }
      }
    }

    if ( this.allRowsFetched && index > this._rowsBuffer.length ) {
      if ( this._rowsBuffer.length == 0 ) {
        return Promise.resolve();
      }
      return this.setDisplayFirstRowIndex(this._rowsBuffer.length-1);
    }

    if ( this.displayFirstRowIndex == index ) {
      return Promise.resolve();
    }

    this._pageSelectedRowIndex = this._displayFirstRowIndex - index + this._pageSelectedRowIndex;

    if ( index + this.rowsPerPage <= this._rowsBuffer.length ) {
      return this._refreshRows(index);
    }

    if (this.allRowsFetched ) {
      return this._refreshRows(index);
    }

    this.lock();

    let startRowIndex   = this._rowsBuffer.length;
    let lastRowIndex    = index + this.rowsPerPage * (this.pagesBufferSize + 1);

    return this.load({
      startRow : startRowIndex,
      endRow   : lastRowIndex
    }).then(() =>  {
      let _index = index;
      if ( _index > this._rowsBuffer.length - 1) {
        _index = this._rowsBuffer.length - 1;
      }
      return this._refreshRows(_index > 0 ? _index : 1);
    });


  }

  get currentPage(): number {
    return this._currentPage;
  }

  setCurrentPage(value: number) : Promise<any> {
    if ( this.locked ) { return; }

    if ( this._currentPage == value ) {
      return;
    }
    this._currentPage   = value;

    this.setDisplayFirstRowIndex(this.rowsPerPage * ( this._currentPage - 1) + 1);
  }


  set currentPage(value: number) {
    this.setCurrentPage(value);
  }

  public scrollUp(count : number)  : Promise<any> {
    if ( this.locked ) { return; }

     return this.setDisplayFirstRowIndex(this.displayFirstRowIndex - count);
  }

  public scrollDown(count : number)  : Promise<any> {
    if ( this.locked ) { return; }

    return this.setDisplayFirstRowIndex(this.displayFirstRowIndex + count);
  }

  public onMouseWheelUp(event){
    if ( this.locked ) { return; }

    this.scrollUp(event.wheelDelta / 120);
  }

  public onMouseWheelDown(event) {
    if ( this.locked ) { return; }

    this.scrollDown(- event.wheelDelta / 120);
  }

  get firstPageText() : string {
    return '«';
  }

  get lastPageText() : string {
    return '»';
  }

  get previousPageText() : string {
    return '<';
  }

  get nextPageText() : string {
    return '>';
  }

  get editRowText() : string {
    return 'Изменить';
  }

  get saveRowText() : string {
    return 'Ок';
  }

  get cancelEditText() : string {
    return 'Отмена';
  }

  openEditView(id : string) {

  }


  editRow(id? : string) {
    if ( id == null ) {
      if ( this.pageSelectedRowIndex == null ) {
        this.clawiz.logError('Cannot edit row for null id and currentRowIndex == null');
        return;
      }
      id = this.rowsDisplayBuffer[this.pageSelectedRowIndex].id;
    }
    this.openEditView(id);
  }


  openAddView() {

  }

  createRow() {
    this.openAddView();
  }

  deleteRow(id? : string) {

    if ( id == null ) {
      if ( this.pageSelectedRowIndex == null ) {
        this.clawiz.logError('Cannot delete row for null id and currentRowIndex == null');
        return;
      }
      id = this.rowsDisplayBuffer[this.pageSelectedRowIndex].id;
    }

    let lastFirstRowIndex = this._displayFirstRowIndex;

    this.clawiz.ui.showYesNoDialog({
        title   : 'Удалить ?',
        message : 'Удалить запись, вы уверены ?',
        defaultNo : true,
        yesCallback : (() => {
          return this.clawiz.api.post({
            path                 : this.apiPath,
            params               : {
              action : 'delete',
              id     : id
            },
            synchronizeAfterCall : false
          })
            .then((response : ApiResponse) => {
              if ( response.success ) {
                return this._reload(lastFirstRowIndex);
              }
            });

        })
      });

  }

  selectRow(index : number, event? : any) : Promise<any> {
    if ( this.locked ) { return; }

    if ( this.rowsDisplayBuffer.length == 0 ) {
      return this.clawiz.getPromiseReject("Cannot select row for empty rowsDisplayBuffer");
    }

    if ( index >= this.rowsDisplayBuffer.length) {
      return this.clawiz.getPromiseReject("Selected row index ? exceed rowsDisplayBuffer size ?", index, this.rowsDisplayBuffer.length);
    }

    this._pageSelectedRowIndex = index;

    for(let link of this._childLinks ) {
      let childLink : GridLink = {
        name : 'parent'
        ,values : [
          {
            columnName : link.values[0].columnName
            ,value     : this.rowsDisplayBuffer[index].id
          }
        ]
      }
      link.grid['setParentLink'](childLink);
    }

  }

  removeRowSelection() : Promise<any>  {
    this._pageSelectedRowIndex = -1;

    for(let link of this._childLinks ) {
      let childLink : GridLink = {
        name : 'parent'
        ,values : [
          {
            columnName : link.values[0].columnName
            ,value : null
          }
        ]
      }
      link.grid['setParentLink'](childLink);
    }

    return Promise.resolve();
  }

  onClickRow(index : number, event? : any) {
    if ( this.locked ) { return; }
    this.selectRow(index, event);
  }

  onDoubleClickRow(index : number, event? : any) {
    if ( this.locked ) { return; }
    this.selectRow(index, event);
    this.editRow();
  }

  editRowInline(index : number, event? : any) {
    if ( index >= this.rowsDisplayBuffer.length ) {
      return this.clawiz.getPromiseReject("Wrong row index ?", index);
    }
    this.removeRowSelection();
    this.rowsDisplayBuffer[index].editing = true;
    event.stopPropagation();
  }


  saveInlineEdit(index : number, event? : any) {
    if ( index >= this.rowsDisplayBuffer.length ) {
      return this.clawiz.getPromiseReject("Wrong row index ?", index);
    }
    this.removeRowSelection();
    this.rowsDisplayBuffer[index].editing = false;
    event.stopPropagation();
  }


  cancelInlineEdit(index : number, event? : any) {
    if ( index >= this.rowsDisplayBuffer.length ) {
      return this.clawiz.getPromiseReject("Wrong row index ?", index);
    }
    this.removeRowSelection();
    this.rowsDisplayBuffer[index].editing = false;
    event.stopPropagation();
  }


  get canCreateRow() : boolean {
    return true;
  }

  get canEditRow() : boolean {
    return this.pageSelectedRowIndex != null && this.pageSelectedRowIndex  >= 0 && this.pageSelectedRowIndex < this.rowsPerPage;
  }

  get canDeleteRow() : boolean {
    return this.pageSelectedRowIndex != null && this.pageSelectedRowIndex  >= 0 && this.pageSelectedRowIndex < this.rowsPerPage;
  }

}
