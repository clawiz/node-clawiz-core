/*
 * Copyright (c) Clawiz
 */


export class GridRowModel {
  id                : string;
  private _editing : boolean;


  get editing(): boolean {
    return this._editing;
  }

  set editing(value: boolean) {
    this._editing = value;
  }
}
