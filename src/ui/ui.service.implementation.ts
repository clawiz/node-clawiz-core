/*
 * Copyright (c) Clawiz
 */

import {AbstractService} from "../service/abstract.service";
import {UiService, Loading, RouterService, YesNoDialogOptions} from "../interfaces";

export class UiServiceImplementation extends AbstractService implements UiService {

  private _router : RouterService;


  get router(): RouterService {
    return this._router;
  }

  set router(value: RouterService) {
    this._router = value;
  }

  openHomePage(): Promise<any> {
    return Promise.resolve(null);
  }

  openLoginPage(): Promise<any> {
    return Promise.resolve(null);
  }

  showLoading(text?: string): Promise<Loading> {
    return Promise.resolve(null);
  }

  showMessageAlert(title: string, text: string, callback?: () => void): Promise<any> {
    return Promise.resolve(null);
  }

  showErrorAlert(message?: string, callback?: () => void): Promise<any> {
    return Promise.resolve(null);
  }

  showYesNoDialog(options : YesNoDialogOptions) : Promise<any> {
    if ( confirm(options.message)) {
      if ( options.yesCallback != null ) {
        options.yesCallback();
      }
    } else {
      if ( options.noCallback != null ) {
        options.noCallback();
      }
    }
    return Promise.resolve(null);
  }
}
