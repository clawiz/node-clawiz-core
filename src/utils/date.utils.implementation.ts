/*
 * Copyright (c) Clawiz
 */

import {DateUtils} from "../interfaces";
import {AbstractService} from "../service/abstract.service";
import * as moment from 'moment';

export class DateUtilsImplementation extends AbstractService  implements DateUtils {

  private _dateFormat     : string = 'DD.MM.YYYY';
  private _dateTimeFormat : string = 'DD.MM.YYYY HH:mm:ss';

  private _ISO8601_DATE_FORMAT      : string = "YYYY-MM-DD";
  private _ISO8601_DATE_TIME_FORMAT : string = "YYYY-MM-DD'T'THH:mm:ss";


  get ISO8601_DATE_FORMAT() {
    return this._ISO8601_DATE_FORMAT;
  }

  get ISO8601_DATE_TIME_FORMAT() {
    return this._ISO8601_DATE_TIME_FORMAT;
  }

  get dateFormat(): string {
    return this._dateFormat;
  }

  get dateTimeFormat(): string {
    return this._dateTimeFormat;
  }

  dateToString(date : Date, format? : string) {
    return date != null ? moment(date).format(format != null ? format : this.dateFormat) : null;
  }

  dateTimeToString(date : Date , format? : string) {
    return date != null ? moment(date).format(format != null ? format : this.dateTimeFormat) : null;
  }

  stringToDate(value : string, format? : string) : Date {
    return value != null ? moment(value, format != null ? format : this.dateFormat).toDate() : null;
  }

  stringToDateTime(value : string, format? : string) : Date {
    return value != null ? moment(value, format != null ? format : this.dateTimeFormat).toDate() : null;
  }




}
