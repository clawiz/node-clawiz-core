/*
 * Copyright (c) Clawiz
 */

import {ClawizError, Clawiz} from "../interfaces";
export class ClawizErrorImplementation implements ClawizError {

  name       : string;
  message    : string;
  stack      : string;

  reason     : Error;
  parameters : any[];

  clawiz     : Clawiz;


  constructor(clawiz: Clawiz) {
    this.clawiz = clawiz;
  }
}
